# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0

stages:
  - Pre-commit Run
  - Build Image
  - Publish Image
  - E2E Testing



.python_setup:
  image: python:${PYTHON_VERSION}
  before_script:
    - pip install --upgrade pip
    - pip install .
  parallel:
    matrix:
      - PYTHON_VERSION: ["3.9", "3.10", "3.11"]


pre_commit_run:
  stage: Pre-commit Run
  extends: .python_setup
  script:
    - pre-commit run --all-files --config=.pre-commit-config.yaml

variables:
  IMAGE_TAR: image.tar

.docker_setup:
  image: docker:26.0
  services:
      - docker:26.0-dind
  before_script:
      - docker login $CI_REGISTRY -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD
  tags:
    - docker

Build Image:
  stage: Build Image
  extends: .docker_setup
  rules:
    - if: '$CI_COMMIT_TAG'
  script:
    - docker build --network=host -t $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG .
    - docker save $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG > $IMAGE_TAR
  artifacts:
    paths:
      - $IMAGE_TAR
    expire_in: 1 week

Push Image:
  stage: Publish Image
  extends: .docker_setup
  rules:
    - if: '$CI_COMMIT_TAG'
  script:
      - docker load < $IMAGE_TAR
      - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG

Push as Latest:
  stage: Publish Image
  extends: .docker_setup
  rules:
    - if: '$CI_COMMIT_TAG =~ /^\d+\.\d+(\.\d+)?(-(pl)\d+){0,1}$/'
  script:
      - docker load < $IMAGE_TAR
      - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG $CI_REGISTRY_IMAGE:latest
      - docker push $CI_REGISTRY_IMAGE:latest


Build Dashboard Image For E2E:
    stage: Build Image
    extends: .docker_setup
    script:
        - IMAGE_NAME=$CI_REGISTRY_IMAGE:e2e-$CI_COMMIT_BRANCH
        - docker build --network=host -t $IMAGE_NAME .
        - docker push $IMAGE_NAME

Build Database Image For E2E:
    stage: Build Image
    extends: .docker_setup
    script:
        - IMAGE_NAME=$CI_REGISTRY_IMAGE:db_e2e-$CI_COMMIT_BRANCH
        - cd mariadb
        - docker build --network=host -t $IMAGE_NAME .
        - docker push $IMAGE_NAME

End to End Tests:
  stage: E2E Testing
  extends: .python_setup
  needs: [
      "Build Dashboard Image For E2E",
      "Build Database Image For E2E"
  ]
  services:
    - name: $CI_REGISTRY_IMAGE:db_e2e-$CI_COMMIT_BRANCH
      alias: dashboard_db
    - name: $CI_REGISTRY_IMAGE:e2e-$CI_COMMIT_BRANCH
      alias: dashboard
  before_script:
    - pip install pytest
    - pip install pytest-playwright playwright -U
    - playwright install
    - playwright install-deps

  variables:
    FF_NETWORK_PER_BUILD: 1
    MARIADB_RANDOM_ROOT_PASSWORD: true
    MARIADB_USER: library_db_hmc
    MARIADB_PASSWORD: 1234
    MARIADB_DATABASE: library_db_hmc
    DB_CONTAINER_NAME_OR_ADDRESS: dashboard_db
    DB_NAME: library_db_hmc
    DB_USER: library_db_hmc
    DB_PASSWORD: 1234
    PORT: 8050
    MAX_YEAR: 2024
    OLD_MAX_YEAR: 2022
    FUJI_HOST: localhost
    FUJI_PORT: 80
    FUJI_PROTOCOL: http
  script:
    - pytest test/test_e2e.py --tracing on  --junitxml=test_report.xml
  tags:
    - docker
  artifacts:
    when: always
    paths:
      - test_report.xml
    reports:
      junit: test_report.xml
