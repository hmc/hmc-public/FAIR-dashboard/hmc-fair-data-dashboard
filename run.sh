#!/bin/bash

# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0

pip install --upgrade pip
pip install -e .

gunicorn --config=gunicorn.conf.py app:server
