# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0
import os

import dash
import i18n
from dash import callback
from dash import dcc
from dash import html
from dash import Input
from dash import Output

from .partials.banner import banner
from .partials.footer import footer
from .partials.home.bottom_info_box import bottom_info_box
from .partials.home.charts import chart
from .partials.home.home_number import home_number
from .partials.navbar import navbar
from .utility.helper import change_legend_according_viewport_size

dash.register_page(__name__, path_template="/<lang>", title="Welcome")


def layout(lang="en"):
    """Complete Home page layout with Navbar, Banner, Page Content and Footer

    Parameters
    ----------
    lang : string
        language part from URL variable path template of the Dash App

    Returns
    -------
    layout for home page
    """
    i18n.set("locale", lang)
    i18n.set("fallback", "en")
    i18n.load_path.append(os.path.abspath("translations"))

    return html.Div(
        [
            # Navbar and Banner
            navbar(),
            banner(),
            # home page self-content
            html.Div(
                [
                    html.H2(i18n.t("home.title"), className="fw-bold pt-5 pb-3"),
                    # top info box
                    html.Div(
                        dcc.Markdown(i18n.t("home.infoContent")),
                        className="card p-5 info-box",
                    ),
                    # Total Count of Literatures and Datasets
                    home_number(),
                    # Chart of total count
                    html.Div(
                        [
                            dcc.Graph(
                                id="graph_literature_dataset",
                                config={"displayModeBar": True},
                            ),
                            html.Div(
                                dcc.Markdown(i18n.t("home.graphDes")),
                                className="p-3 text-justify small",
                            ),
                        ],
                        className="card",
                    ),
                    # read on info box
                    html.Div(
                        dcc.Markdown(i18n.t("home.readOn")),
                        className="card p-3 info-box",
                    ),
                    # FAQ info box
                    bottom_info_box(),
                ],
                className="container",
            ),
            # Footer part
            footer(),
        ],
    )


@callback(
    Output("graph_literature_dataset", "figure"),
    Input("viewport_container", "data"),
)
def update_graph_home_figures(viewport_size):
    viewport_size_width = viewport_size["width"]
    try:
        return change_legend_according_viewport_size(
            chart(viewport_size_width), viewport_size_width
        )

    except (RuntimeError, TypeError, NameError, AttributeError):
        return None
