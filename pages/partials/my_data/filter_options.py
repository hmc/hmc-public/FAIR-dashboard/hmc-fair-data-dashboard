# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0
import dash_bootstrap_components as dbc
import i18n
from dash import html


def filter_options():
    """Filter options for My Data Page"""
    return html.Div(
        [
            dbc.Row(
                [
                    dbc.Col(
                        html.Div(
                            dbc.Input(
                                id="input_on_submit",
                                type="text",
                                placeholder=i18n.t("my_data.PIDLabel"),
                            )
                        ),
                        md=11,
                    ),
                    dbc.Col(
                        html.Div(
                            dbc.Button(
                                i18n.t("my_data.search"),
                                id="search_btn",
                                class_name="my-btn",
                                color="primary",
                                n_clicks=0,
                            ),
                        ),
                        md=1,
                    ),
                ],
                className="mt-2 pb-2 px-4 align-items-md-stretch",
            )
        ],
        className="sticky-top card",
        style={"top": "55px"},
    )
