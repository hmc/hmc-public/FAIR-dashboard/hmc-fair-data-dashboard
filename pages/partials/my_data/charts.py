# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0
import dash_bootstrap_components as dbc
import i18n
import numpy as np
import pandas as pd
import plotly.express as px
from dash import dcc
from dash import html

from ...utility.colors import colors_hmc
from ...utility.db import get_dataframe_per_doi
from ...utility.fuji_local.fuji_scorer import get_fuji_score
from ...utility.fuji_local.scholix_info import get_scholix_info
from ...utility.helper import get_data_fair_by_publisher_per_doi
from ...utility.helper import get_fair_scores_per_doi
from ...utility.helper import is_valid_doi
from ...utility.helper import update_graph_layout_min_vertical
from ...utility.helper import update_graph_layout_my_data_fair


def _process_not_fount_doi_db(_doi):
    return html.Div(
        [
            html.Div(
                [
                    html.Div(
                        [
                            dbc.Alert(
                                [
                                    html.H4(
                                        i18n.t("my_data.go_live_not_found_msg_title"),
                                        className="alert-heading",
                                    ),
                                    dcc.Markdown(
                                        i18n.t("my_data.go_live_not_found_msg_body"),
                                        link_target="_blank",
                                    ),
                                ],
                                color="warning",
                            ),
                            html.Div(
                                dbc.Button(
                                    i18n.t("my_data.go_live_btn"),
                                    id="go_live_btn",
                                    n_clicks=0,
                                    color="primary",
                                    class_name="my-btn",
                                ),
                                className="d-grid gap-2",
                            ),
                        ]
                    )
                ],
                className="card p-5 ",
            ),
        ]
    )


def get_my_data(value, live=False):
    if value:  # check the value if it is existed
        if is_valid_doi(value):  # check if the value is valid DOI
            value = value.lower()
            if live:
                return process_live_assessment(value)

            db_status, data_df = get_dataframe_per_doi(value)
            if db_status:
                return get_fair_chart_html_with_json_result(data_df)
            return None, _process_not_fount_doi_db(value)
    return None, html.Div(
        [
            html.Div(
                [
                    html.Div(
                        [
                            dbc.Alert(
                                [
                                    html.H4(
                                        i18n.t("my_data.not_valid_doi_msg_title"),
                                        className="alert-heading",
                                    ),
                                    dcc.Markdown(
                                        i18n.t("my_data.not_valid_doi_msg_body"),
                                        link_target="_blank",
                                    ),
                                ],
                                color="warning",
                            ),
                        ]
                    )
                ],
                className="card p-5 ",
            )
        ]
    )


def process_live_assessment(value):
    print(value)
    # get information of the PID that is typed by user
    info = get_scholix_info(value)
    if info is None:
        print("info not found")
        result = html.Div(
            [
                html.Div(
                    [
                        dbc.Alert(
                            [
                                html.H4(
                                    i18n.t("my_data.live_not_accepted_type_msg_title"),
                                    className="alert-heading",
                                ),
                                dcc.Markdown(
                                    i18n.t("my_data.live_not_accepted_type_msg_body"),
                                    link_target="_blank",
                                ),
                            ],
                            color="warning",
                        ),
                    ],
                    className="card p-5 ",
                ),
            ]
        )

    # print("info found")
    elif "dataset" == info["Type"] or "collection" == info["Type"]:
        print("info found, and type is dataset or collection")
        # if the type of result is Dataset or Collection
        # Do the fuji assessment

        data_df = get_fuji_score(value, 1, info)
        if data_df is not None:
            return get_fair_chart_html_with_json_result(data_df, True)

        print("info found, fuji score failed")
        alerts = html.Div(
            [
                dbc.Alert(
                    dcc.Markdown(
                        i18n.t("my_data.live_fuji_timeout"), link_target="_blank"
                    ),
                    color="warning",
                ),
            ]
        )

        result = html.Div([html.Div(alerts)], className="card p-5 ")
    else:
        print("info found, and type is not dataset or collection")
        result = html.Div(
            [
                html.Div(
                    [
                        dbc.Alert(
                            [
                                html.H4(
                                    i18n.t("my_data.live_not_accepted_type_msg_title"),
                                    className="alert-heading",
                                ),
                                dcc.Markdown(
                                    i18n.t("my_data.live_not_accepted_type_msg_body"),
                                    link_target="_blank",
                                ),
                            ],
                            color="warning",
                        ),
                        html.Div(
                            [
                                html.H5(
                                    i18n.t("my_data.datasetTitle")
                                    + f": {info['Title']}"
                                ),
                                html.H5(
                                    i18n.t("my_data.publisher")
                                    + f": {info['Publisher']}"
                                ),
                                html.H5(
                                    i18n.t("my_data.publicationYear")
                                    + f": {info['Publication Year']}"
                                ),
                                html.H5(i18n.t("my_data.type") + f": {info['Type']}"),
                            ]
                        ),
                    ],
                    className="card p-5 ",
                ),
            ]
        )

    return None, result


def get_fair_chart_html_with_json_result(data_df, live=False):
    if len(data_df) > 0:
        dataframe_fair = get_fair_scores_per_doi(data_df).round(decimals=2)
        convert_type = {
            "F": float,
            "F1": float,
            "F2": float,
            "F3": float,
            "F4": float,
            "A": float,
            "A1": float,
            "I": float,
            "I1": float,
            "I2": float,
            "I3": float,
            "R": float,
            "R1": float,
            "R1_1": float,
            "R1_2": float,
            "R1_3": float,
        }
        dataframe_fair = dataframe_fair.astype(convert_type)
        filtered_df_fair = get_data_fair_by_publisher_per_doi(dataframe_fair).round(
            decimals=2
        )

        data = {
            "FAIR principle": [
                i18n.t("my_data.findability"),
                i18n.t("my_data.accessibility"),
                i18n.t("my_data.interoperability"),
                i18n.t("my_data.reusability"),
            ],
            "Score": list(filtered_df_fair[["F", "A", "I", "R"]].iloc[0, :]),
            "Title": np.repeat(filtered_df_fair["Title"][0], 4),
        }

        filtered_df = pd.DataFrame(data=data)

        ##############################################################

        graph_fair_my_data = px.bar(
            filtered_df,
            x="FAIR principle",
            y="Score",
            color="FAIR principle",
            title=i18n.t("my_data.fairChartTitle"),
            height=600,
            labels={
                "FAIR principle": i18n.t("my_data.FAIR_principle"),
            },
            hover_data=["Title"],
        )

        # graph_fair_my_data.update_traces(
        #     hovertemplate = "<b>"
        #                   + i18n.t('my_data.score') +
        #                   ": </b> %{y} %"
        # )
        # overwrite tick labels
        graph_fair_my_data.update_layout(
            xaxis={"title": i18n.t("my_data.FAIR_chart_title"), "automargin": True},
            yaxis={"showticklabels": True, "title": i18n.t("my_data.F-UJIscore")},
            legend_traceorder="grouped",
            transition_duration=500,
            title_x=0.5,
            title_y=0.9,
            font_family="halvar",
            margin={"b": 10},
            autosize=True,
            height=600,
            hovermode="x unified",
            hoverlabel={"font_family": "halvar"},
        )

        ###############################################################

        local_live = dbc.Alert(
            [
                html.H4(
                    i18n.t("my_data.local_fuji_result_msg_title"),
                    className="alert-heading",
                ),
                dcc.Markdown(
                    i18n.t("my_data.local_fuji_result_msg_body"), link_target="_blank"
                ),
            ]
        )

        alert_live = dbc.Alert(
            [
                html.H4(
                    i18n.t("my_data.live_fuji_result_msg_title"),
                    className="alert-heading",
                ),
                dcc.Markdown(
                    i18n.t("my_data.live_fuji_result_msg_body"), link_target="_blank"
                ),
            ]
        )
        ###############################################################
        result = html.Div(
            [
                html.Div(
                    [
                        html.Div(alert_live if live else local_live),
                        html.Div(
                            [
                                html.H5(
                                    i18n.t("my_data.averageFAIRscore")
                                    + f": {filtered_df_fair['FAIR'][0]} %"
                                ),
                                html.H5(
                                    i18n.t("my_data.datasetTitle")
                                    + f": {filtered_df_fair['Title'][0]}"
                                ),
                                html.H5(
                                    i18n.t("my_data.publisher")
                                    + f": {filtered_df_fair['Publisher'][0]}"
                                ),
                                html.H5(
                                    i18n.t("my_data.publicationYear")
                                    + f": {filtered_df_fair['Publication Year'][0]}"
                                ),
                                html.H5(
                                    i18n.t("my_data.type")
                                    + f": {filtered_df_fair['Type'][0]}"
                                ),
                            ]
                        ),
                    ],
                    className="card p-5 ",
                ),
                html.Div(
                    [
                        html.Div(
                            [
                                dcc.Graph(
                                    id="graph_fair_my_data",
                                    figure=graph_fair_my_data,
                                    config={"displayModeBar": True},
                                ),
                                html.Div(
                                    [
                                        html.Div(
                                            dcc.Markdown(
                                                i18n.t("my_data.fairChartDes")
                                            ),
                                            className="p-3 text-justify small",
                                        ),
                                    ],
                                    className="d-flex flex-wrap",
                                ),
                            ],
                            className="card col-md-12 col-sm-12",
                        ),
                    ],
                    className="row",
                ),
                html.Div(
                    [
                        html.Div(
                            dbc.RadioItems(
                                options=[
                                    {
                                        "label": i18n.t("repo.findableWithDetails"),
                                        "value": "f",
                                    },
                                    {
                                        "label": i18n.t("repo.accessibleWithDetails"),
                                        "value": "a",
                                    },
                                    {
                                        "label": i18n.t(
                                            "repo.interoperableWithDetails"
                                        ),
                                        "value": "i",
                                    },
                                    {
                                        "label": i18n.t("repo.reusableWithDetails"),
                                        "value": "r",
                                    },
                                ],
                                value="f",
                                id="assess_my_data_fair_mode",
                                className="btn-group",
                                inputClassName="btn-check",
                                labelClassName="btn my-btn-color",
                                labelCheckedClassName="my-btn-active",
                                # className="d-flex flex-wrap p-3",
                            ),
                            className="radio-group d-flex justify-content-center bd-highlight",
                        )
                    ],
                    className="card p-5",
                ),
                html.Div(id="graph_details_graph"),
            ]
        )

        return filtered_df_fair.to_json(), result

    result = html.Div(
        [
            html.Div(
                [
                    html.Div(
                        [
                            html.H5(i18n.t("my_data.PID_not_found")),
                        ]
                    )
                ],
                className="card p-5 ",
            ),
        ]
    )

    return None, result


def f_chart(filtered_df_json):
    # converting json dataset from dictionary to dataframe
    dataframe = pd.DataFrame.from_dict(filtered_df_json)
    dataframe.reset_index(level=0, inplace=True)

    data = {
        "FAIR principle": ["F", "F1", "F2", "F3", "F4"],
        "Score": list(dataframe[["F", "F1", "F2", "F3", "F4"]].iloc[0, :]),
        "Title": np.repeat(dataframe["Title"][0], 5),
    }
    filtered_df = pd.DataFrame(data=data)
    graph = px.bar(
        filtered_df,
        y="FAIR principle",
        x="Score",
        color="FAIR principle",
        title=i18n.t("my_data.findability"),
        color_discrete_map=colors_hmc,
        height=600,
        labels={"variable": i18n.t("my_data.FAIR_principle")},
        orientation="h",
        hover_name=[
            i18n.t("data.Findable"),
            i18n.t("data.F1"),
            i18n.t("data.F2"),
            i18n.t("data.F3"),
            i18n.t("data.F4"),
        ],
    )

    update_graph_layout_my_data_fair(
        graph,
        i18n.t("data.Score"),
        i18n.t("my_data.F-UJIscore"),
        i18n.t("my_data.FAIR_chart_title"),
    )
    update_graph_layout_min_vertical(graph)

    result = html.Div(
        [
            html.Div(
                [
                    dcc.Graph(
                        figure=graph,
                        config={"displayModeBar": True},
                    ),
                ],
                className="card col-md-7 col-sm-12",
            ),
            html.Div(
                [
                    html.Div(
                        [
                            dcc.Markdown(
                                i18n.t("my_data.findabilityDes"), link_target="_blank"
                            ),
                            html.H4(i18n.t("my_data.FAIR_test")),
                            html.Ul(
                                [
                                    html.Li(i18n.t("data.F")),
                                    html.Li(i18n.t("data.F1")),
                                    html.Li(i18n.t("data.F2")),
                                    html.Li(i18n.t("data.F3")),
                                    html.Li(i18n.t("data.F4")),
                                ]
                            ),
                        ]
                    )
                ],
                className="card p-5 info-box col-md-5 col-sm-12",
            ),
        ],
        className="row",
    )
    return result


def a_chart(filtered_df_json):
    # converting json dataset from dictionary to dataframe
    dataframe = pd.DataFrame.from_dict(filtered_df_json)
    dataframe.reset_index(level=0, inplace=True)

    data = {
        "FAIR principle": ["A", "A1"],
        "Score": list(dataframe[["A", "A1"]].iloc[0, :]),
        "Title": np.repeat(dataframe["Title"][0], 2),
    }
    filtered_df = pd.DataFrame(data=data)
    graph = px.bar(
        filtered_df,
        y="FAIR principle",
        x="Score",
        color="FAIR principle",
        title=i18n.t("my_data.accessibility"),
        color_discrete_map=colors_hmc,
        height=600,
        barmode="group",
        labels={"variable": i18n.t("my_data.FAIR_principle")},
        hover_name=[
            i18n.t("data.Accessible"),
            i18n.t("data.A1"),
        ],
    )

    update_graph_layout_my_data_fair(
        graph,
        i18n.t("my_data.score"),
        i18n.t("my_data.F-UJIscore"),
        i18n.t("my_data.FAIR_chart_title"),
    )

    update_graph_layout_min_vertical(graph)
    ###############################################################
    result = html.Div(
        [
            html.Div(
                [
                    dcc.Graph(
                        figure=graph,
                        config={"displayModeBar": True},
                    ),
                ],
                className="card col-md-7 col-sm-12",
            ),
            html.Div(
                [
                    html.Div(
                        [
                            dcc.Markdown(
                                i18n.t("my_data.accessibilityDes"), link_target="_blank"
                            ),
                            html.H4(i18n.t("my_data.FAIR_test")),
                            html.Ul(
                                [
                                    html.Li(i18n.t("data.A")),
                                    html.Li(i18n.t("data.A1")),
                                ]
                            ),
                        ]
                    )
                ],
                className="card p-5 info-box col-md-5 col-sm-12",
            ),
        ],
        className="row",
    )
    return result


def i_chart(filtered_df_json):
    # converting json dataset from dictionary to dataframe
    dataframe = pd.DataFrame.from_dict(filtered_df_json)
    dataframe.reset_index(level=0, inplace=True)

    data = {
        "FAIR principle": ["I", "I1", "I2", "I3"],
        "Score": list(dataframe[["I", "I1", "I2", "I3"]].iloc[0, :]),
        "Title": np.repeat(dataframe["Title"][0], 4),
    }
    filtered_df = pd.DataFrame(data=data)
    graph = px.bar(
        filtered_df,
        y="FAIR principle",
        x="Score",
        color="FAIR principle",
        title=i18n.t("my_data.interoperability"),
        color_discrete_map=colors_hmc,
        height=600,
        labels={"variable": i18n.t("my_data.FAIR_principle")},
        hover_name=[
            i18n.t("data.Interoperable"),
            i18n.t("data.I1"),
            i18n.t("data.I2"),
            i18n.t("data.I3"),
        ],
    )

    update_graph_layout_my_data_fair(
        graph,
        i18n.t("my_data.score"),
        i18n.t("my_data.F-UJIscore"),
        i18n.t("my_data.FAIR_chart_title"),
    )

    update_graph_layout_min_vertical(graph)

    ###############################################################

    result = html.Div(
        [
            html.Div(
                [
                    dcc.Graph(
                        figure=graph,
                        config={"displayModeBar": True},
                    ),
                ],
                className="card col-md-7 col-sm-12",
            ),
            html.Div(
                [
                    html.Div(
                        [
                            dcc.Markdown(
                                i18n.t("my_data.interoperabilityDes"),
                                link_target="_blank",
                            ),
                            html.H4(i18n.t("my_data.FAIR_test")),
                            html.Ul(
                                [
                                    html.Li(i18n.t("data.I")),
                                    html.Li(i18n.t("data.I1")),
                                    html.Li(i18n.t("data.I2")),
                                    html.Li(i18n.t("data.I3")),
                                ]
                            ),
                        ]
                    )
                ],
                className="card p-5 info-box col-md-5 col-sm-12",
            ),
        ],
        className="row",
        id="graph_i_my_data",
    )
    return result


def r_chart(filtered_df_json):
    # converting json dataset from dictionary to dataframe
    dataframe = pd.DataFrame.from_dict(filtered_df_json)
    dataframe.reset_index(level=0, inplace=True)

    data = {
        "FAIR principle": ["R", "R1", "R1.1", "R1.2", "R1.3"],
        "Score": list(dataframe[["R", "R1", "R1_1", "R1_2", "R1_3"]].iloc[0, :]),
        "Title": np.repeat(dataframe["Title"][0], 5),
    }
    filtered_df = pd.DataFrame(data=data)
    graph = px.bar(
        filtered_df,
        y="FAIR principle",
        x="Score",
        color="FAIR principle",
        title=i18n.t("my_data.reusability"),
        color_discrete_map=colors_hmc,
        height=600,
        labels={"variable": i18n.t("my_data.FAIR_principle")},
        hover_name=[
            i18n.t("data.Reusable"),
            i18n.t("data.R1"),
            i18n.t("data.R11"),
            i18n.t("data.R12"),
            i18n.t("data.R13"),
        ],
    )

    update_graph_layout_my_data_fair(
        graph,
        i18n.t("my_data.score"),
        i18n.t("my_data.F-UJIscore"),
        i18n.t("my_data.FAIR_chart_title"),
    )

    update_graph_layout_min_vertical(graph)
    ###############################################################
    result = html.Div(
        [
            html.Div(
                [
                    dcc.Graph(
                        figure=graph,
                        config={"displayModeBar": True},
                    ),
                ],
                className="card col-md-7 col-sm-12",
            ),
            html.Div(
                [
                    html.Div(
                        [
                            dcc.Markdown(
                                i18n.t("my_data.reusableDes"), link_target="_blank"
                            ),
                            html.H4(i18n.t("my_data.FAIR_test")),
                            html.Ul(
                                [
                                    html.Li(i18n.t("data.R")),
                                    html.Li(i18n.t("data.R1")),
                                    html.Li(i18n.t("data.R11")),
                                    html.Li(i18n.t("data.R12")),
                                    html.Li(i18n.t("data.R13")),
                                ]
                            ),
                        ]
                    )
                ],
                className="card p-5 info-box col-md-5 col-sm-12",
            ),
        ],
        className="row",
    )
    return result
