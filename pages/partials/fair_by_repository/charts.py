# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0
import i18n
import plotly.express as px
import plotly.graph_objects as go

from ...utility.colors import colors_hmc
from ...utility.helper import get_sum_dataset_by_center_year
from ...utility.helper import get_sum_dataset_by_rf_year
from ...utility.helper import list_fair
from ...utility.helper import update_graph_layout_min
from ...utility.helper import update_graph_layout_no_filter_match
from ...utility.helper import warp_graph_title_according_viewport_size
from ..data_in_helmholtz.charts import fair_sunburst


def graph_research_field_in_repository(filtered_df):
    research_fields_in_repo_df = get_sum_dataset_by_rf_year(filtered_df).sort_values(
        "Number of Dataset", ascending=False
    )

    ###################################################################
    fig = px.bar(
        research_fields_in_repo_df,
        x="Publication Year",
        y="Number of Dataset",
        color="Research Field",
        title=i18n.t("repo.graphResearchFieldInRepositoryTitle"),
        color_discrete_map=colors_hmc,
        height=600,
        barmode="stack",
        labels={
            "Publication Year": i18n.t("repo.publicationYearLabel"),
            "Number of Dataset": i18n.t("repo.NumberOfDatasetsLabel"),
            "Research Field": i18n.t("repo.researchFieldLabel"),
        },
    )

    update_graph_layout_min(fig)

    if research_fields_in_repo_df.empty:
        update_graph_layout_no_filter_match(fig, i18n.t("repo.noFilterMatch"))

    warp_graph_title_according_viewport_size(
        fig, i18n.t("repo.graphResearchFieldInRepositoryTitle")
    )

    return fig


def graph_centers_in_repository(filtered_df):
    repository_center_year_df = get_sum_dataset_by_center_year(filtered_df).sort_values(
        "Number of Dataset", ascending=False
    )

    fig = px.bar(
        repository_center_year_df,
        x="Publication Year",
        y="Number of Dataset",
        color="Center",
        title=i18n.t("repo.graphCentersInRepositoryTitle"),
        color_discrete_map=colors_hmc,
        height=600,
        barmode="stack",
        labels={
            "Center": i18n.t("repo.centerLabel"),
            "Publication Year": i18n.t("repo.publicationYearLabel"),
            "Number of Dataset": i18n.t("repo.NumberOfDatasetsLabel"),
        },
    )

    update_graph_layout_min(fig)

    if repository_center_year_df.empty:
        update_graph_layout_no_filter_match(fig, i18n.t("repo.noFilterMatch"))

    warp_graph_title_according_viewport_size(
        fig, i18n.t("repo.graphCentersInRepositoryTitle")
    )

    return fig


def graph_average_fair_score_sunburst(filtered_fair):
    return fair_sunburst(
        filtered_fair,
        i18n.t("repo.graphAverageFAIRscoreSunburstInRepositoryTitle"),
        i18n.t("repo.noFilterMatch"),
    )


def graph_dataset_publication_fair_line(filtered_fair, fair_mode):
    filtered_df_fair = (
        filtered_fair.groupby(["Publication Year"])[list_fair]
        .mean()
        .reset_index()
        .round(decimals=2)
    )

    hovertemplate = (
        "<b>" + i18n.t("repo.publicationYearLabel") + ": </b> %{x} <br>"
        "<b>%{meta[0]}: </b> %{y:.1f} %<extra></extra>"
    )

    if fair_mode == "fair":
        fig = go.Figure(
            [
                go.Scatter(
                    name=i18n.t("data.Findable"),
                    x=filtered_df_fair["Publication Year"],
                    y=filtered_df_fair["F"],
                    mode="markers+lines",
                    meta=["Findable"],
                    hovertemplate=hovertemplate,
                    showlegend=True,
                ),
                go.Scatter(
                    name=i18n.t("data.Accessible"),
                    x=filtered_df_fair["Publication Year"],
                    y=filtered_df_fair["A"],
                    mode="markers+lines",
                    meta=["Accessible"],
                    hovertemplate=hovertemplate,
                    showlegend=True,
                ),
                go.Scatter(
                    name=i18n.t("data.Interoperable"),
                    x=filtered_df_fair["Publication Year"],
                    y=filtered_df_fair["I"],
                    mode="markers+lines",
                    meta=["Interoperable"],
                    hovertemplate=hovertemplate,
                    showlegend=True,
                ),
                go.Scatter(
                    name=i18n.t("data.Reusable"),
                    x=filtered_df_fair["Publication Year"],
                    y=filtered_df_fair["R"],
                    mode="markers+lines",
                    meta=["Reusable"],
                    hovertemplate=hovertemplate,
                    showlegend=True,
                ),
            ]
        )
    elif fair_mode == "f":
        fig = go.Figure(
            [
                go.Scatter(
                    name=i18n.t("data.Findable"),
                    x=filtered_df_fair["Publication Year"],
                    y=filtered_df_fair["F"],
                    mode="markers+lines",
                    meta=[i18n.t("data.Findable")],
                    hovertemplate=hovertemplate,
                    showlegend=True,
                ),
                go.Scatter(
                    name="F1",
                    x=filtered_df_fair["Publication Year"],
                    y=filtered_df_fair["F1"],
                    mode="markers+lines",
                    meta=[i18n.t("data.F1")],
                    hovertemplate=hovertemplate,
                    showlegend=True,
                ),
                go.Scatter(
                    name="F2",
                    x=filtered_df_fair["Publication Year"],
                    y=filtered_df_fair["F2"],
                    mode="markers+lines",
                    meta=[i18n.t("data.F2")],
                    hovertemplate=hovertemplate,
                    showlegend=True,
                ),
                go.Scatter(
                    name="F3",
                    x=filtered_df_fair["Publication Year"],
                    y=filtered_df_fair["F3"],
                    mode="markers+lines",
                    meta=[i18n.t("data.F3")],
                    hovertemplate=hovertemplate,
                    showlegend=True,
                ),
                go.Scatter(
                    name="F4",
                    x=filtered_df_fair["Publication Year"],
                    y=filtered_df_fair["F4"],
                    mode="markers+lines",
                    meta=[i18n.t("data.F4")],
                    hovertemplate=hovertemplate,
                    showlegend=True,
                ),
            ]
        )
    elif fair_mode == "a":
        fig = go.Figure(
            [
                go.Scatter(
                    name=i18n.t("data.Accessible"),
                    x=filtered_df_fair["Publication Year"],
                    y=filtered_df_fair["A"],
                    mode="markers+lines",
                    meta=[i18n.t("data.Accessible")],
                    hovertemplate=hovertemplate,
                    showlegend=True,
                ),
                go.Scatter(
                    name="A1",
                    x=filtered_df_fair["Publication Year"],
                    y=filtered_df_fair["A1"],
                    mode="markers+lines",
                    meta=[i18n.t("data.A1")],
                    hovertemplate=hovertemplate,
                    showlegend=True,
                ),
            ]
        )
    elif fair_mode == "i":
        fig = go.Figure(
            [
                go.Scatter(
                    name=i18n.t("data.Interoperable"),
                    x=filtered_df_fair["Publication Year"],
                    y=filtered_df_fair["I"],
                    mode="markers+lines",
                    meta=[i18n.t("data.Interoperable")],
                    hovertemplate=hovertemplate,
                    showlegend=True,
                ),
                go.Scatter(
                    name="I1",
                    x=filtered_df_fair["Publication Year"],
                    y=filtered_df_fair["I1"],
                    mode="markers+lines",
                    meta=[i18n.t("data.I1")],
                    hovertemplate=hovertemplate,
                    showlegend=True,
                ),
                go.Scatter(
                    name="I2",
                    x=filtered_df_fair["Publication Year"],
                    y=filtered_df_fair["I2"],
                    mode="markers+lines",
                    meta=[i18n.t("data.I2")],
                    hovertemplate=hovertemplate,
                    showlegend=True,
                ),
                go.Scatter(
                    name="I3",
                    x=filtered_df_fair["Publication Year"],
                    y=filtered_df_fair["I3"],
                    mode="markers+lines",
                    meta=[i18n.t("data.I3")],
                    hovertemplate=hovertemplate,
                    showlegend=True,
                ),
            ]
        )
    elif fair_mode == "r":
        fig = go.Figure(
            [
                go.Scatter(
                    name=i18n.t("data.Reusable"),
                    x=filtered_df_fair["Publication Year"],
                    y=filtered_df_fair["R"],
                    mode="markers+lines",
                    meta=[i18n.t("data.Reusable")],
                    hovertemplate=hovertemplate,
                    showlegend=True,
                ),
                go.Scatter(
                    name="R1",
                    x=filtered_df_fair["Publication Year"],
                    y=filtered_df_fair["R1"],
                    mode="markers+lines",
                    meta=[i18n.t("data.R1")],
                    hovertemplate=hovertemplate,
                    showlegend=True,
                ),
                go.Scatter(
                    name="R1.1",
                    x=filtered_df_fair["Publication Year"],
                    y=filtered_df_fair["R1_1"],
                    mode="markers+lines",
                    meta=[i18n.t("data.R11")],
                    hovertemplate=hovertemplate,
                    showlegend=True,
                ),
                go.Scatter(
                    name="R1.2",
                    x=filtered_df_fair["Publication Year"],
                    y=filtered_df_fair["R1_2"],
                    mode="markers+lines",
                    meta=[i18n.t("data.R12")],
                    hovertemplate=hovertemplate,
                    showlegend=True,
                ),
                go.Scatter(
                    name="R1.3",
                    x=filtered_df_fair["Publication Year"],
                    y=filtered_df_fair["R1_3"],
                    mode="markers+lines",
                    meta=[i18n.t("data.R13")],
                    hovertemplate=hovertemplate,
                    showlegend=True,
                ),
            ]
        )

    fig.update_xaxes(
        tickwidth=4,
        tickfont={
            "family": "halvar",
            "size": 11,
        },
    )
    fig.update_layout(
        yaxis={"automargin": True},
        margin={"b": 10},
        autosize=True,
        height=600,
        transition_duration=500,
        font_family="halvar",
        hoverlabel={"font_family": "halvar"},
        yaxis_title=i18n.t("data.AverageFAIR"),
        xaxis_title=i18n.t("repo.publicationYearLabel"),
        legend_title=i18n.t("repo.indicator"),
        title=i18n.t("repo.graphAverageFAIRscoreLineInRepositoryTitle"),
        title_x=0.5,
        title_y=0.9,
        hovermode="x unified",
    )

    warp_graph_title_according_viewport_size(
        fig, i18n.t("repo.graphAverageFAIRscoreLineInRepositoryTitle")
    )
    return fig
