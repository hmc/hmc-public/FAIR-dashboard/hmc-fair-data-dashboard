# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0
import textwrap

import i18n
from dash import dcc
from dash import html

from ...utility.db import publisher_df


def filter_options():
    """Filter options for Repositories Page"""
    return html.Div(
        [
            html.Div(
                [
                    html.Div(
                        [
                            html.Div(
                                i18n.t("repo.repositories"), className="menu-title"
                            ),
                            dcc.Dropdown(
                                id="repository_filter",
                                options=[
                                    {
                                        "label": textwrap.shorten(
                                            publisher, width=100, placeholder="..."
                                        ),
                                        "value": publisher,
                                    }
                                    for publisher in publisher_df["Publisher"]
                                ],
                                value="ZENODO",
                                clearable=False,
                                className="dropdown",
                                style={"width": "100%", "verticalAlign": "middle"},
                            ),
                        ],
                        className="flex-fill",
                    ),
                    html.Div(
                        html.Div(
                            [
                                html.Br(),
                                html.Div(
                                    [
                                        html.Img(
                                            src="/assets/images/icons/warning_sign.svg",
                                            width=25,
                                        ),
                                        html.P(
                                            i18n.t("data.filter_msg"),
                                            className="m-0",
                                        ),
                                    ],
                                    className="d-flex pt-md-3 text-info",
                                ),
                            ]
                        ),
                        id="filter_msg_fair_by_repository_page",
                        className="filter_msg flex-fill",
                    ),
                ],
                className="pt-4 pb-1 px-4 d-flex ",
            ),
        ],
        className="sticky-top card",
    )
