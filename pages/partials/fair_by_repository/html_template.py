# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0
import dash_bootstrap_components as dbc
import i18n
from dash import dcc
from dash import html


def charts():
    return html.Div(
        [
            html.Div(
                [
                    dcc.Graph(
                        id="graph_research_field_in_repository",
                        config={"displayModeBar": True},
                    ),
                    html.Div(
                        dcc.Markdown(i18n.t("repo.graphResearchFieldInRepositoryDes")),
                        className="p-3 text-justify small",
                    ),
                ],
                className="card",
            ),
            html.Div(
                [
                    dcc.Graph(
                        id="graph_centers_in_repository",
                        config={"displayModeBar": True},
                    ),
                    html.Div(
                        dcc.Markdown(i18n.t("repo.graphCentersInRepositoryDes")),
                        className="p-3 text-justify small",
                    ),
                ],
                className="card",
            ),
            html.Div(
                [
                    html.Div(
                        [
                            html.H4(
                                "",
                                className="avg_FAIR_text",
                                id="avg_FAIR_text_by_repository",
                            ),
                            dcc.Graph(
                                id="graph_average_FAIR_score_sunburst_in_repository",
                                config={"displayModeBar": True},
                            ),
                            html.Div(
                                dcc.Markdown(
                                    i18n.t(
                                        "repo.graphAverageFAIRscoreSunburstInRepositoryDes"
                                    )
                                ),
                                className="p-3 text-justify small",
                            ),
                        ],
                        className="card col-md-7 col-sm-12",
                    ),
                    html.Div(
                        [
                            html.Div(
                                [
                                    html.Img(
                                        src="/assets/images/vertical_FAIR.png",
                                        className="rounded w-75 h-auto mx-auto d-block",
                                    ),
                                ]
                            )
                        ],
                        className="card p-1 info-box col-md-5 col-sm-12",
                    ),
                ],
                className=" row ",
            ),
            html.Div(
                [
                    dcc.Graph(
                        id="graph_average_FAIR_score_line_in_repository",
                        config={"displayModeBar": True},
                    ),
                    html.Div(
                        [
                            html.Div(
                                [
                                    dbc.RadioItems(
                                        options=[
                                            {
                                                "label": i18n.t(
                                                    "repo.averageFAIRScore"
                                                ),
                                                "value": "fair",
                                            },
                                            {
                                                "label": i18n.t(
                                                    "repo.findableWithDetails"
                                                ),
                                                "value": "f",
                                            },
                                            {
                                                "label": i18n.t(
                                                    "repo.accessibleWithDetails"
                                                ),
                                                "value": "a",
                                            },
                                            {
                                                "label": i18n.t(
                                                    "repo.interoperableWithDetails"
                                                ),
                                                "value": "i",
                                            },
                                            {
                                                "label": i18n.t(
                                                    "repo.reusableWithDetails"
                                                ),
                                                "value": "r",
                                            },
                                        ],
                                        value="fair",
                                        id="fair_mode",
                                        switch=True,
                                        inline=True,
                                        className="d-flex flex-wrap p-3",
                                    ),
                                    html.Div(
                                        dcc.Markdown(
                                            i18n.t(
                                                "repo.graphAverageFAIRscoreLineInRepositoryDes"
                                            )
                                        ),
                                        className="p-3 text-justify small",
                                    ),
                                ],
                                className="d-flex flex-row-reverse flex-wrap",
                            ),
                        ],
                        className="card",
                    ),
                ]
            ),
        ]
    )
