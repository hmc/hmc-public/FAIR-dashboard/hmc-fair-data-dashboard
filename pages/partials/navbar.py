# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0
import dash_bootstrap_components as dbc
import i18n
from dash import callback
from dash import html
from dash import Input
from dash import Output
from dash import State


def navbar():
    return dbc.Navbar(
        dbc.Container(
            [
                html.A(
                    dbc.Row(
                        [
                            dbc.Col(
                                dbc.NavbarBrand(
                                    html.Img(
                                        src="/assets/images/logos/HMC_Logo_RGB_Blue.svg",
                                        height="45px",
                                        id="logo",
                                    ),
                                    className="ms-2",
                                )
                            ),
                        ],
                        align="center",
                        className="g-0",
                    ),
                    href="https://helmholtz-metadaten.de/en",
                    style={"textDecoration": "none"},
                ),
                dbc.NavbarToggler(id="navbar-toggler", n_clicks=0),
                dbc.Collapse(
                    [
                        dbc.Nav(
                            [
                                dbc.NavItem(
                                    dbc.NavLink("EN", href="/en", active="partial")
                                ),
                                dbc.NavItem(
                                    dbc.NavLink("DE", href="/de", active="partial")
                                ),
                            ],
                            className="ms-auto",
                            id="languages",
                        ),
                        dbc.Nav(
                            [
                                html.P(
                                    [
                                        i18n.t("nav.languages"),
                                        html.Span(
                                            [
                                                dbc.NavLink(
                                                    "EN", href="/en", active="partial"
                                                ),
                                                dbc.NavLink(
                                                    "DE", href="/de", active="partial"
                                                ),
                                            ]
                                        ),
                                    ],
                                    className="d-flex justify-content-between",
                                ),
                                html.Hr(),
                            ],
                            className="ms-auto p-4",
                            id="languages_mobile",
                        ),
                        dbc.Nav(
                            [
                                dbc.NavLink(
                                    i18n.t("nav.welcome"),
                                    href=i18n.t("nav.welcomeLink"),
                                    active="exact",
                                ),
                                dbc.NavLink(
                                    i18n.t("nav.dataInHelmholtz"),
                                    href=i18n.t("nav.dataInHelmholtzLink"),
                                    active="exact",
                                ),
                                dbc.NavLink(
                                    i18n.t("nav.repositories"),
                                    href=i18n.t("nav.repositoriesLink"),
                                    active="exact",
                                ),
                                dbc.NavLink(
                                    i18n.t("nav.myData"),
                                    href=i18n.t("nav.myDataLink"),
                                    active="exact",
                                ),
                                dbc.NavLink(
                                    i18n.t("nav.about"),
                                    href=i18n.t("nav.aboutLink"),
                                    active="exact",
                                ),
                            ],
                            className="ms-auto",
                            navbar=True,
                        ),
                    ],
                    id="navbar-collapse",
                    navbar=True,
                ),
            ],
        ),
        color="#fff",
        id="navbar",
        className="sticky-top",
    )


# add callback for toggling the collapse on small screens
@callback(
    Output("navbar-collapse", "is_open"),
    [Input("navbar-toggler", "n_clicks")],
    [State("navbar-collapse", "is_open")],
)
def toggle_navbar_collapse(clicks, is_open):
    return not is_open if clicks else is_open
