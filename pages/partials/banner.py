# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0
import i18n
from dash import dcc
from dash import html


def banner():
    return html.Div(
        [
            html.H3(
                [dcc.Markdown(i18n.t("nav.bannerSlogan"))],
                style={"flex": "1"},
                className="py-0",
            ),
            html.Img(
                src="/assets/images/logos/Dashboard_Logo_inverted.svg",
                style={"marginRight": "10%", "height": "50%"},
            ),
        ],
        className="banner d-flex flex-row align-items-center",
        style={"backgroundImage": 'url("/assets/images/Banner.png")'},
    )
