# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0
import dash_bootstrap_components as dbc
import i18n
from dash import html


def footer():
    return html.Footer(
        html.Div(
            [
                html.Div(
                    [
                        html.Div(
                            [
                                html.A(
                                    html.Img(
                                        src="/assets/images/logos/helmholtz_logo.svg"
                                    ),
                                    href="/",
                                    className="block",
                                ),
                            ],
                            className="d-flex flex-column",
                        ),
                        html.Div(
                            [html.P(i18n.t("nav.slogan"), className="text-light")]
                        ),
                    ],
                    className="d-flex flex-row flex-wrap justify-content-between py-4",
                ),
                html.Div(
                    [
                        html.Div(
                            [
                                dbc.Nav(
                                    [
                                        html.A(
                                            html.Img(
                                                src="/assets/images/icons/bluesky_Logo.svg",
                                                width="24",
                                            ),
                                            href="https://bsky.app/profile/helmholtzhmc.bsky.social",
                                            style={"textDecoration": "none"},
                                            className="nav-link",
                                            target="_blank",
                                        ),
                                        html.A(
                                            html.Img(
                                                src="/assets/images/icons/linkedin.svg",
                                                width="24",
                                            ),
                                            href="https://www.linkedin.com/company/helmholtz-metadata-collaboration-hmc",
                                            style={"textDecoration": "none"},
                                            className="nav-link",
                                            target="_blank",
                                        ),
                                        html.A(
                                            html.Img(
                                                src="/assets/images/icons/mattermost.svg",
                                                width="24",
                                            ),
                                            href="https://mattermost.hzdr.de/hmc-public",
                                            style={"textDecoration": "none"},
                                            className="nav-link",
                                            target="_blank",
                                        ),
                                        html.A(
                                            html.Img(
                                                src="/assets/images/icons/mastodon.svg",
                                                width="24",
                                            ),
                                            href="https://helmholtz.social/@helmholtz_hmc",
                                            style={"textDecoration": "none"},
                                            className="nav-link",
                                            target="_blank",
                                        ),
                                    ],
                                ),
                            ],
                            className="d-flex",
                        ),
                        html.Div(
                            [
                                html.P(
                                    i18n.t("nav.bannerSlogan"),
                                    className="text-light pt-4",
                                )
                            ]
                        ),
                    ],
                    className="d-flex flex-column flex-wrap justify-content-between pt-4",
                ),
                html.Div(
                    [
                        html.Div(
                            [
                                dbc.Nav(
                                    [
                                        html.A(
                                            i18n.t("nav.imprint"),
                                            href=i18n.t("nav.imprintLink"),
                                            style={"textDecoration": "none"},
                                            className="nav-link col-12 col-md-4",
                                            target="_blank",
                                        ),
                                        html.A(
                                            i18n.t("nav.dataProtectionDeclaration"),
                                            href=i18n.t(
                                                "nav.dataProtectionDeclarationLink"
                                            ),
                                            style={"textDecoration": "none"},
                                            className="nav-link col-12 col-md-4",
                                            target="_blank",
                                        ),
                                        html.A(
                                            i18n.t("nav.contact"),
                                            href=i18n.t("nav.contactLink"),
                                            style={"textDecoration": "none"},
                                            className="nav-link col-12 col-md-4",
                                            target="_blank",
                                        ),
                                    ],
                                    className="ms-auto row",
                                    navbar=True,
                                ),
                            ],
                            className="",
                        ),
                        html.Div(
                            [
                                html.Div(
                                    "© " + i18n.t("nav.organization"),
                                    className="text-white text-sm",
                                ),
                            ],
                            className="",
                        ),
                    ],
                    className="d-flex flex-wrap justify-content-between",
                ),
            ],
            className="container p-2",
        ),
        className="p-3 bg-blue-dark",
    )
