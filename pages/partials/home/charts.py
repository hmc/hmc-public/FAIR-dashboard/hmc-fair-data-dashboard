# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0
import os

import i18n
import pandas as pd
import plotly.graph_objects as go
from dotenv import find_dotenv
from dotenv import load_dotenv

from ...utility.db import data_count_df as data_df
from ...utility.db import literature_count_df
from ...utility.helper import update_graph_layout
from ...utility.helper import warp_graph_title_according_viewport_size

load_dotenv(find_dotenv())

MAX_YEAR = int(os.environ.get("MAX_YEAR", "2024"))
OLD_MAX_YEAR = int(os.environ.get("OLD_MAX_YEAR", "2022"))


def chart(viewport_size_width):
    literature_for_join_df = literature_count_df[
        ["Publication Type", "Publication Year", "Total Number of Literatures"]
    ].copy()
    # literature_for_join_df = get_count_by_year(literature_for_join_df, 'Total Number of Literatures')
    data_for_join_df = data_df[
        ["Publication Type", "Publication Year", "Total Number of Datasets"]
    ].copy()

    result = pd.merge(
        literature_for_join_df, data_for_join_df, on="Publication Year", how="outer"
    )

    result[["Total Number of Datasets", "Total Number of Literatures"]] = result[
        ["Total Number of Datasets", "Total Number of Literatures"]
    ].astype(float)

    result["Ratio, Datasets / Literatures"] = (
        result["Total Number of Datasets"] / result["Total Number of Literatures"]
    ).round(decimals=2)

    old_years_result = result[result["Publication Year"] <= OLD_MAX_YEAR]
    current_year_result = result[
        (result["Publication Year"] >= OLD_MAX_YEAR)
        & (result["Publication Year"] <= MAX_YEAR)
    ]

    fig1 = go.Figure()
    fig1.add_traces(
        go.Scatter(
            name=i18n.t("home.totalNumberLiterature"),
            x=old_years_result["Publication Year"],
            y=old_years_result["Total Number of Literatures"],
            mode="markers+lines",
            showlegend=True,
            hovertemplate="<b>"
            + i18n.t("home.publicationYear")
            + ": </b> %{x} <br><b>"
            + i18n.t("home.totalNumberLiterature")
            + ": </b> %{y}"
            + "<extra></extra>",
            line={"color": "darkblue", "dash": "solid"},
        )
    )

    fig1.add_traces(
        go.Scatter(
            name=i18n.t("home.totalNumberDatasets"),
            x=old_years_result["Publication Year"],
            y=old_years_result["Total Number of Datasets"],
            mode="markers+lines",
            hovertemplate="<b>"
            + i18n.t("home.publicationYear")
            + ": </b> %{x} <br><b>"
            + i18n.t("home.totalNumberDatasets")
            + ": </b> %{y}"
            + "<extra></extra>",
            showlegend=True,
            line={"color": "brown", "dash": "solid"},
        )
    )

    fig1.add_traces(
        go.Scatter(
            name=i18n.t("home.totalNumberLiteratureInProgress"),
            x=current_year_result["Publication Year"],
            y=current_year_result["Total Number of Literatures"],
            mode="markers+lines",
            showlegend=True,
            hovertemplate="<b>"
            + i18n.t("home.publicationYear")
            + ": </b> %{x} <br><b>"
            + i18n.t("home.totalNumberLiterature")
            + ": </b> %{y}"
            + "<extra></extra>",
            line={"color": "darkblue", "dash": "dot"},
        )
    )

    fig1.add_traces(
        go.Scatter(
            name=i18n.t("home.totalNumberDatasetsInProgress"),
            x=current_year_result["Publication Year"],
            y=current_year_result["Total Number of Datasets"],
            mode="markers+lines",
            hovertemplate="<b>"
            + i18n.t("home.publicationYear")
            + ": </b> %{x} <br><b>"
            + i18n.t("home.totalNumberDatasets")
            + ": </b> %{y}"
            + "<extra></extra>",
            showlegend=True,
            line={"color": "brown", "dash": "dot"},
        )
    )

    fig1.update_traces(textposition="top center")
    update_graph_layout(
        fig1,
        i18n.t("home.chartTitle"),
        i18n.t("home.totalNumber"),
        i18n.t("home.publicationYear"),
        "v",
    )

    warp_graph_title_according_viewport_size(
        fig1, i18n.t("home.chartTitle"), viewport_size_width
    )

    return fig1
