# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0
import dash_bootstrap_components as dbc
import i18n
from dash import html

from ...utility.db import center_df
from ...utility.db import data_count_df
from ...utility.db import last_updated_info_df
from ...utility.db import literature_count_df
from ...utility.helper import get_last_update_date


def home_number():
    return html.Div(
        [
            html.Div(
                [
                    html.Div(
                        [
                            html.Div(
                                [
                                    html.Div(
                                        [
                                            html.Div(
                                                [
                                                    html.Div(
                                                        i18n.t(
                                                            "home.totalNumberLiteratureCount"
                                                        ),
                                                        className="text-xs fonts-weight-bold text-primary text-uppercase mb-1",
                                                    ),
                                                    html.Div(
                                                        [
                                                            literature_count_df[
                                                                "Total Number of Literatures"
                                                            ].sum()
                                                        ],
                                                        className="h5 mb-0 fonts-weight-bold text-gray-800",
                                                    ),
                                                ],
                                                className="col mr-2",
                                            ),
                                            html.Div(
                                                [
                                                    html.Img(
                                                        src="/assets/images/icons/literature.svg",
                                                        width=50,
                                                    ),
                                                ],
                                                className="col-auto",
                                            ),
                                        ],
                                        className="row no-gutters align-items-center",
                                    ),
                                ],
                                className="card-body",
                            ),
                        ],
                        className="card border-left-primary h-100 py-2",
                    ),
                ],
                className="col-xl-3 col-md-6 mb-4",
                id="total-number-literature-count",
                n_clicks=0,
            ),
            dbc.Popover(
                [
                    dbc.PopoverHeader(
                        i18n.t("home.totalNumberLiteratureCountPopoverTitle")
                    ),
                    dbc.PopoverBody(
                        i18n.t("home.totalNumberLiteratureCountPopoverDesc")
                    ),
                ],
                target="total-number-literature-count",
                # body=True,
                placement="auto",
                trigger="hover",
            ),
            ###
            html.Div(
                [
                    html.Div(
                        [
                            html.Div(
                                [
                                    html.Div(
                                        [
                                            html.Div(
                                                [
                                                    html.Div(
                                                        i18n.t(
                                                            "home.totalNumberDatasetCount"
                                                        ),
                                                        className="text-xs fonts-weight-bold text-primary text-uppercase mb-1",
                                                    ),
                                                    html.Div(
                                                        [
                                                            data_count_df[
                                                                "Total Number of Datasets"
                                                            ].sum()
                                                        ],
                                                        className="h5 mb-0 fonts-weight-bold text-gray-800",
                                                    ),
                                                ],
                                                className="col mr-2",
                                            ),
                                            html.Div(
                                                [
                                                    html.Img(
                                                        src="/assets/images/icons/dataset.svg",
                                                        width=50,
                                                    ),
                                                ],
                                                className="col-auto",
                                            ),
                                        ],
                                        className="row no-gutters align-items-center",
                                    ),
                                ],
                                className="card-body",
                            ),
                        ],
                        className="card border-left-primary h-100 py-2",
                    ),
                ],
                className="col-xl-3 col-md-6 mb-4",
                id="total-number-dataset-count",
                n_clicks=0,
            ),
            dbc.Popover(
                [
                    dbc.PopoverHeader(
                        i18n.t("home.totalNumberDatasetCountPopoverTitle")
                    ),
                    dbc.PopoverBody(i18n.t("home.totalNumberDatasetCountPopoverDesc")),
                ],
                target="total-number-dataset-count",
                # body=True,
                placement="auto",
                trigger="hover",
            ),
            ###
            html.Div(
                [
                    html.Div(
                        [
                            html.Div(
                                [
                                    html.Div(
                                        [
                                            html.Div(
                                                [
                                                    html.Div(
                                                        i18n.t(
                                                            "home.centerConnectedCount"
                                                        ),
                                                        className="text-xs fonts-weight-bold text-primary text-uppercase mb-1",
                                                    ),
                                                    html.Div(
                                                        [len(center_df.index)],
                                                        className="h5 mb-0 fonts-weight-bold text-gray-800",
                                                    ),
                                                ],
                                                className="col mr-2",
                                            ),
                                            html.Div(
                                                [
                                                    html.Img(
                                                        src="/assets/images/icons/centers.svg",
                                                        width=50,
                                                    ),
                                                ],
                                                className="col-auto",
                                            ),
                                        ],
                                        className="row no-gutters align-items-center",
                                    ),
                                ],
                                className="card-body",
                            ),
                        ],
                        className="card border-left-primary h-100 py-2",
                    ),
                ],
                className="col-xl-3 col-md-6 mb-4",
                id="centers-connected",
                n_clicks=0,
            ),
            dbc.Popover(
                [
                    dbc.PopoverHeader(i18n.t("home.centerConnectedCountPopoverTitle")),
                    dbc.PopoverBody(
                        html.Div(
                            [
                                i18n.t("home.centerConnectedCountPopoverDesc"),
                                html.Ul(
                                    [html.Li(center) for center in center_df["Name"]]
                                ),
                            ]
                        )
                    ),
                ],
                target="centers-connected",
                # body=True,
                placement="auto",
                trigger="hover",
            ),
            ###
            html.Div(
                [
                    html.Div(
                        [
                            html.Div(
                                [
                                    html.Div(
                                        [
                                            html.Div(
                                                [
                                                    html.Div(
                                                        i18n.t(
                                                            "home.lastUpdatedInfoDate"
                                                        ),
                                                        className="text-xs fonts-weight-bold text-primary text-uppercase mb-1",
                                                    ),
                                                    html.Div(
                                                        [
                                                            get_last_update_date(
                                                                last_updated_info_df[
                                                                    "max_last_updated_info"
                                                                ]
                                                            ),
                                                        ],
                                                        className="h5 mb-0 fonts-weight-bold text-gray-800",
                                                    ),
                                                ],
                                                className="col mr-2",
                                            ),
                                            html.Div(
                                                [
                                                    html.Img(
                                                        src="/assets/images/icons/info_update.svg",
                                                        width=50,
                                                    ),
                                                ],
                                                className="col-auto",
                                            ),
                                        ],
                                        className="row no-gutters align-items-center",
                                    ),
                                ],
                                className="card-body",
                            ),
                        ],
                        className="card border-left-primary h-100 py-2",
                    ),
                ],
                className="col-xl-3 col-md-6 mb-4",
                id="last_updated_info",
                n_clicks=0,
            ),
            dbc.Popover(
                [
                    dbc.PopoverHeader(i18n.t("home.lastUpdatedInfoDateTitle")),
                    dbc.PopoverBody(i18n.t("home.lastUpdatedInfoDateDesc")),
                ],
                target="last_updated_info",
                # body=True,
                placement="auto",
                trigger="hover",
            ),
        ],
        className="row",
    )
