# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0
import dash_bootstrap_components as dbc
import i18n
from dash import dcc
from dash import html


def bottom_info_box():
    return html.Div(
        [
            html.H3(i18n.t("faq.title")),
            dbc.Accordion(
                [
                    dbc.AccordionItem(
                        dcc.Markdown(i18n.t("faq.useThisDashboardContent")),
                        title=i18n.t("faq.useThisDashboard"),
                    ),
                    dbc.AccordionItem(
                        dcc.Markdown(i18n.t("faq.statisticsObtainedContent")),
                        title=i18n.t("faq.statisticsObtained"),
                    ),
                    dbc.AccordionItem(
                        dcc.Markdown(i18n.t("faq.downloadDataContent")),
                        title=i18n.t("faq.downloadData"),
                    ),
                    dbc.AccordionItem(
                        dcc.Markdown(i18n.t("faq.reuseCodeDashboardContent")),
                        title=i18n.t("faq.reuseCodeDashboard"),
                    ),
                    dbc.AccordionItem(
                        dcc.Markdown(
                            i18n.t("faq.suitableRepositoryContent"),
                            link_target="_blank",
                        ),
                        title=i18n.t("faq.suitableRepository"),
                    ),
                    dbc.AccordionItem(
                        dcc.Markdown(
                            i18n.t("faq.FAIRprinciplesContent"), link_target="_blank"
                        ),
                        title=i18n.t("faq.FAIRprinciples"),
                    ),
                    dbc.AccordionItem(
                        dcc.Markdown(
                            i18n.t("faq.roleHelmholtzAssociationContent"),
                            link_target="_blank",
                        ),
                        title=i18n.t("faq.roleHelmholtzAssociation"),
                    ),
                    dbc.AccordionItem(
                        dcc.Markdown(
                            i18n.t("faq.F-UJIworkContent"), link_target="_blank"
                        ),
                        title=i18n.t("faq.F-UJIwork"),
                    ),
                    dbc.AccordionItem(
                        dcc.Markdown(
                            i18n.t("faq.interpretingFAIRscoresContent"),
                            link_target="_blank",
                        ),
                        title=i18n.t("faq.interpretingFAIRscores"),
                    ),
                    dbc.AccordionItem(
                        dcc.Markdown(
                            i18n.t("faq.improveTheFAIRnessContent"),
                            link_target="_blank",
                        ),
                        title=i18n.t("faq.improveTheFAIRness"),
                    ),
                    dbc.AccordionItem(
                        dcc.Markdown(i18n.t("faq.contactHMCContent")),
                        title=i18n.t("faq.contactHMC"),
                    ),
                ],
                flush=True,
            ),
        ],
        className="card info-box",
    )
