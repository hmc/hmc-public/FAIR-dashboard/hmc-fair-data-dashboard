# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0
import i18n
import plotly.express as px

from ...utility.colors import colors_hmc
from ...utility.helper import get_sum_dataset_by_rf_repositories
from ...utility.helper import get_sum_dataset_by_rf_repositories_year
from ...utility.helper import list_fair_total
from ...utility.helper import update_graph_layout_no_filter_match
from ...utility.helper import warp_graph_title_according_viewport_size


def graph_repository_usage(filtered_df):
    repository_usage_df = get_sum_dataset_by_rf_repositories(filtered_df).sort_values(
        "Number of Dataset", ascending=False
    )

    repository_usage_df["ShortPublisherName"] = (
        repository_usage_df["Publisher"]
        .apply(lambda x: (x[:20] + "...") if len(x) > 20 else x)
        .tolist()
    )

    print(repository_usage_df)

    fig = px.bar(
        repository_usage_df,
        x="Number of Dataset",
        y="ShortPublisherName",
        color="Research Field",
        title=i18n.t("data.graphRepositoryUsageTitle"),
        color_discrete_map=colors_hmc,
        height=600,
        barmode="stack",
        custom_data=[
            repository_usage_df["Publisher"],
            repository_usage_df["Research Field"],
        ],
        labels={
            "ShortPublisherName": i18n.t("data.publishersLabel"),
            "Number of Dataset": i18n.t("data.NumberOfDatasetsLabel"),
            "Research Field": i18n.t("data.researchFieldLabel"),
        },
    )

    fig.update_traces(
        hovertemplate="<b>"
        + i18n.t("data.publisherLabel")
        + ":"
        + " </b> %{customdata[0]} "
        + "<br><b>"
        + i18n.t("data.researchFieldLabel")
        + ":"
        + "</b> %{customdata[1]} "
        + "<br><b>"
        + i18n.t("data.NumberOfDatasetsLabel")
        + ":"
        + "</b> %{x} <extra></extra>"
    )

    fig.update_layout(
        transition_duration=500,
        title_x=0.5,
        title_y=0.9,
        font_family="halvar",
        xaxis={"autorange": True, "automargin": True},
        yaxis={
            "autorange": "reversed",
            "automargin": True,
            "categoryorder": "total descending",
        },
        margin={"b": 10},
        autosize=True,
        height=600,
        hovermode="y unified",
        hoverlabel={"font_family": "halvar"},
    )

    if repository_usage_df.empty:
        update_graph_layout_no_filter_match(fig, i18n.t("data.noFilterMatch"))

    warp_graph_title_according_viewport_size(
        fig, i18n.t("data.graphRepositoryUsageTitle")
    )
    return fig


def graph_repository_usage_over_time(filtered_df):
    repository_usage_df_over_time = get_sum_dataset_by_rf_repositories_year(
        filtered_df
    ).sort_values("Number of Dataset", ascending=False)

    repository_usage_df_over_time["ShortPublisherName"] = (
        repository_usage_df_over_time["Publisher"]
        .apply(lambda x: (x[:20] + "...") if len(x) > 20 else x)
        .tolist()
    )

    fig = px.bar(
        repository_usage_df_over_time,
        x="Publication Year",
        y="Number of Dataset",
        color="ShortPublisherName",
        title=i18n.t("data.graphRepositoryOvertimeTitle"),
        color_discrete_map=colors_hmc,
        height=600,
        barmode="stack",
        custom_data=[repository_usage_df_over_time["Publisher"]],
        labels={
            "ShortPublisherName": i18n.t("data.publishersLabel"),
            "Number of Dataset": i18n.t("data.NumberOfDatasetsLabel"),
            "Publication Year": i18n.t("data.publicationYearLabel"),
        },
    )

    fig.update_traces(
        hovertemplate="<b>"
        + i18n.t("data.publisherLabel")
        + ":"
        + " </b> %{customdata[0]} "
        + "<br><b>"
        + i18n.t("data.NumberOfDatasetsLabel")
        + ":"
        + "</b> %{y} <extra></extra>"
    )

    fig.update_layout(
        legend_title_text=i18n.t("data.publishersLabel"),
        transition_duration=500,
        title_x=0.5,
        title_y=0.9,
        font_family="halvar",
        yaxis={"autorange": True, "automargin": True},
        xaxis={"autorange": True, "automargin": True},
        margin={"b": 10},
        autosize=True,
        height=600,
        hovermode="x unified",
        hoverlabel={"font_family": "halvar"},
    )

    if repository_usage_df_over_time.empty:
        update_graph_layout_no_filter_match(fig, i18n.t("data.noFilterMatch"))

    warp_graph_title_according_viewport_size(
        fig, i18n.t("data.graphRepositoryOvertimeTitle")
    )
    return fig


def graph_average_fair_score_sunburst(filtered_fair):
    return fair_sunburst(
        filtered_fair,
        i18n.t("data.graphAverageFAIRScoreSunburstTitle"),
        i18n.t("data.noFilterMatch"),
    )


def fair_sunburst(filtered_fair, title, no_data_found):
    filtered_df_fair = (
        filtered_fair[list_fair_total]
        .mean()
        .reset_index(name="Avg FAIR")
        .round(decimals=2)
    )

    filtered_df_fair["Avg FAIR"] = filtered_df_fair["Avg FAIR"].astype(float)

    index_avg_total_list = [
        "Total FAIR",
        "Total F",
        "Total F1",
        "Total F2",
        "Total F3",
        "Total F4",
        "Total A",
        "Total A1",
        "Total I",
        "Total I1",
        "Total I2",
        "Total I3",
        "Total R",
        "Total R1",
        "Total R1_1",
        "Total R1_2",
        "Total R1_3",
    ]

    index_avg_score_list = [
        "FAIR",
        "F",
        "F1",
        "F2",
        "F3",
        "F4",
        "A",
        "A1",
        "I",
        "I1",
        "I2",
        "I3",
        "R",
        "R1",
        "R1_1",
        "R1_2",
        "R1_3",
    ]

    filtered_df_fair.set_index("index", inplace=True)
    avg_fair = str(round(filtered_df_fair.loc["FAIR"]["Avg FAIR"], 1))

    score_values = (filtered_df_fair.loc[index_avg_score_list]["Avg FAIR"]).round(
        decimals=1
    )
    weight_values = (filtered_df_fair.loc[index_avg_total_list]["Avg FAIR"]).round(
        decimals=1
    )
    weight_values_percentage = (
        filtered_df_fair.loc[index_avg_total_list]["Avg FAIR"] * 4.1666
    ).round(decimals=1)

    data = {
        "name": [
            "FAIR",
            "F",
            "F1",
            "F2",
            "F3",
            "F4",
            "A",
            "A1",
            "I",
            "I1",
            "I2",
            "I3",
            "R",
            "R1",
            "R1.1",
            "R1.2",
            "R1.3",
        ],
        "parent": [
            "",
            "FAIR",
            "F",
            "F",
            "F",
            "F",
            "FAIR",
            "A",
            "FAIR",
            "I",
            "I",
            "I",
            "FAIR",
            "R",
            "R",
            "R",
            "R",
        ],
        "value": weight_values,
        "score": score_values,
        "value_percent": weight_values_percentage,
    }

    fig = px.sunburst(
        names=data["name"],
        parents=data["parent"],
        values=data["value"],
        color=data["score"],
        branchvalues="total",
        custom_data=[data["value_percent"]],
        color_continuous_scale=px.colors.diverging.RdYlGn,
        template="seaborn",
        range_color=[0, 100],
        labels={"color": i18n.t("data.FujiScoreLabel")},
        hover_name=[
            i18n.t("data.AverageFAIR"),
            i18n.t("data.Findable"),
            i18n.t("data.F1"),
            i18n.t("data.F2"),
            i18n.t("data.F3"),
            i18n.t("data.F4"),
            i18n.t("data.Accessible"),
            i18n.t("data.A1"),
            i18n.t("data.Interoperable"),
            i18n.t("data.I1"),
            i18n.t("data.I2"),
            i18n.t("data.I3"),
            i18n.t("data.Reusable"),
            i18n.t("data.R1"),
            i18n.t("data.R11"),
            i18n.t("data.R12"),
            i18n.t("data.R13"),
        ],
    )

    hovertemplate = "<b>%{hovertext} </b><br />"
    hovertemplate += "<br /><b>" + i18n.t("data.Score") + ": </b> %{color:.1f} % "
    hovertemplate += (
        "<br><b>" + i18n.t("data.Weight") + ": </b> %{customdata} % <extra></extra>"
    )
    fig.update_traces(hovertemplate=hovertemplate)

    fig.update_traces(textinfo="label")
    fig.update_traces(sort=False, selector={"type": "sunburst"})

    formatted_title = title + (" - " if avg_fair == "nan" else " - " + avg_fair + " %")
    fig.update_layout(
        yaxis={"automargin": True},
        margin={"b": 10},
        autosize=True,
        height=550,
        transition_duration=500,
        title_x=0.5,
        title_y=0.9,
        title=formatted_title,
        font_family="halvar",
        hoverlabel={"font_family": "halvar"},
    )

    if avg_fair == "nan":
        update_graph_layout_no_filter_match(fig, no_data_found)

    warp_graph_title_according_viewport_size(fig, formatted_title)

    return fig
