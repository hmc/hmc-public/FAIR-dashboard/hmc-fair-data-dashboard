# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0
import i18n
from dash import dcc
from dash import html

from ...utility.db import center_df
from ...utility.db import research_field_df


def filter_options():
    """Filter options for Data in Helmholtz Page"""
    return html.Div(
        [
            html.Div(
                [
                    html.Div(
                        [
                            html.Div(
                                i18n.t("data.researchFieldLabel"),
                                className="menu-title",
                            ),
                            dcc.Dropdown(
                                id="research_field_filter",
                                options=[
                                    {
                                        "label": i18n.t("data.allResearchField"),
                                        "value": "all",
                                    }
                                ]
                                + [
                                    {"label": research_field, "value": research_field}
                                    for research_field in research_field_df["Name"]
                                ],
                                value="all",
                                clearable=False,
                            ),
                        ]
                    ),
                    html.Div(
                        [
                            html.Div(
                                i18n.t("data.centerLabel"), className="menu-title"
                            ),
                            dcc.Dropdown(
                                id="center_filter",
                                options=[
                                    {"label": i18n.t("data.allCenter"), "value": "all"}
                                ]
                                + [
                                    {"label": center, "value": center}
                                    for center in center_df["Name"]
                                ],
                                value="all",
                                clearable=False,
                            ),
                        ]
                    ),
                    html.Div(
                        html.Div(
                            [
                                html.Br(),
                                html.Div(
                                    [
                                        html.Img(
                                            src="/assets/images/icons/warning_sign.svg",
                                            width=25,
                                        ),
                                        html.P(
                                            i18n.t("data.filter_msg"),
                                            className="m-0",
                                        ),
                                    ],
                                    className="d-flex pt-md-3 text-info",
                                ),
                            ]
                        ),
                        id="filter_msg_data_in_helmholtz_page",
                        className="filter_msg",
                    ),
                ],
                className="d-flex flex-row justify-content-center flex-wrap pt-4 pb-1",
            ),
        ],
        className="sticky-top card ",
    )
