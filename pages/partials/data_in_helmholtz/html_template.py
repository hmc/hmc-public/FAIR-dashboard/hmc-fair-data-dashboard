# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0
import i18n
from dash import dcc
from dash import html


def charts():
    return html.Div(
        [
            html.Div(
                [
                    dcc.Graph(
                        id="graph_repository_usage",
                        config={"displayModeBar": True},
                    ),
                    html.Div(
                        dcc.Markdown(i18n.t("data.graphRepositoryUsageDes")),
                        className="p-3 text-justify small",
                    ),
                ],
                className="card",
            ),
            html.Div(
                [
                    dcc.Graph(
                        id="graph_repository_usage_over_time",
                        config={"displayModeBar": True},
                    ),
                    html.Div(
                        dcc.Markdown(i18n.t("data.graphRepositoryOvertimeDes")),
                        className="p-3 text-justify small",
                    ),
                ],
                className="card",
            ),
            html.Div(
                [
                    html.Div(
                        [
                            html.H4("", className="avg_FAIR_text", id="avg_FAIR_text"),
                            dcc.Graph(
                                id="graph_average_FAIR_score_sunburst",
                                config={"displayModeBar": True},
                            ),
                            html.Div(
                                dcc.Markdown(
                                    i18n.t("data.graphAverageFAIRScoreSunburstDes"),
                                    link_target="_blank",
                                ),
                                className="p-3 text-justify small",
                            ),
                        ],
                        className="card col-md-7 col-sm-12",
                    ),
                    html.Div(
                        [
                            html.Div(
                                [
                                    html.Img(
                                        src="/assets/images/vertical_FAIR.png",
                                        className="rounded w-75 h-auto mx-auto d-block",
                                    ),
                                ]
                            )
                        ],
                        className="card p-1 info-box col-md-5 col-sm-12",
                    ),
                ],
                className=" row ",
            ),
        ]
    )
