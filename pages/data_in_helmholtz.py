# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0
import os

import dash
import i18n
from dash import callback
from dash import dcc
from dash import html
from dash import Input
from dash import Output

from .partials.banner import banner
from .partials.data_in_helmholtz.bottom_info_box import bottom_info_box
from .partials.data_in_helmholtz.charts import graph_average_fair_score_sunburst
from .partials.data_in_helmholtz.charts import graph_repository_usage
from .partials.data_in_helmholtz.charts import graph_repository_usage_over_time
from .partials.data_in_helmholtz.filter_options import filter_options
from .partials.data_in_helmholtz.html_template import charts
from .partials.footer import footer
from .partials.navbar import navbar
from .utility.db import data_is_supplemented_by_df as data_df
from .utility.helper import change_legend_according_viewport_size
from .utility.helper import get_count_dataset_by_rf_repositories_center
from .utility.helper import get_fair_scores_by_centers_research_field

dash.register_page(
    __name__, path_template="/<lang>/data-in-helmholtz", title="Data in Helmholtz"
)

data_df = data_df[data_df["Publication Year"] >= 2000]
dataframe_FAIR = get_fair_scores_by_centers_research_field(data_df)


def layout(lang="en"):
    """Complete Data in Helmholtz page layout with Navbar, Banner, Page Content and Footer

    Parameters
    ----------
    lang : string
        language part from URL variable path template of the Dash App

    Returns
    -------
    layout for Data in Helmholtz page
    """
    i18n.set("locale", lang)
    i18n.set("fallback", "en")
    i18n.load_path.append(os.path.abspath("translations"))

    return html.Div(
        [
            # Navbar and Banner
            navbar(),
            banner(),
            # Data in Helmholtz page self-content
            html.Div(
                [
                    html.H2(i18n.t("data.title"), className="fw-bold pt-5 pb-3"),
                    # top info box
                    html.Div(
                        dcc.Markdown(i18n.t("data.infoContent"), link_target="_blank"),
                        className="card p-5 info-box",
                    ),
                    filter_options(),
                    charts(),
                    # read on info box
                    html.Div(
                        dcc.Markdown(i18n.t("data.readOn")),
                        className="card p-3 info-box",
                    ),
                    bottom_info_box(),
                ],
                className="container-xl",
            ),
            # Footer part
            footer(),
        ],
    )


def sort_higher_publisher(data_frame, high_publisher):
    top_data_df = data_frame.copy()

    top_data_df.loc[
        (~top_data_df["Publisher"].isin(high_publisher["Publisher"]), "Publisher")
    ] = "Others"

    dataframe_rf = get_count_dataset_by_rf_repositories_center(top_data_df)
    return dataframe_rf


@callback(
    Output("graph_repository_usage", "figure"),
    Output("graph_repository_usage_over_time", "figure"),
    Output("graph_average_FAIR_score_sunburst", "figure"),
    Output("filter_msg_data_in_helmholtz_page", component_property="style"),
    Input("center_filter", "value"),
    Input("research_field_filter", "value"),
    Input("viewport_container", "data"),
)
def update_graph_research_data_helmholtz_figures(center, research_field, viewport_size):
    viewport_size_width = viewport_size["width"]
    is_filtered = False
    if center == "all" and research_field == "all":
        high_publisher = (
            data_df.groupby("Publisher")
            .size()
            .reset_index(name="Number of Dataset")
            .sort_values("Number of Dataset", ascending=False)
            .head(10)
        )
        dataframe_rf = sort_higher_publisher(data_df, high_publisher)

        filtered_df = dataframe_rf.sort_values("Number of Dataset", ascending=False)
        filtered_fair = dataframe_FAIR.copy()
        is_filtered = False
    elif center == "all":
        high_publisher = (
            data_df[(data_df["Research Field"] == research_field)]
            .groupby("Publisher")
            .size()
            .reset_index(name="Number of Dataset")
            .sort_values("Number of Dataset", ascending=False)
            .head(10)
        )
        dataframe_rf = sort_higher_publisher(data_df, high_publisher)

        filtered_df = dataframe_rf[
            (dataframe_rf["Research Field"] == research_field)
        ].sort_values("Number of Dataset", ascending=False)
        filtered_fair = dataframe_FAIR[
            (dataframe_FAIR["Research Field"] == research_field)
        ]
        is_filtered = True
    elif research_field == "all":
        high_publisher = (
            data_df[(data_df["Center"] == center)]
            .groupby("Publisher")
            .size()
            .reset_index(name="Number of Dataset")
            .sort_values("Number of Dataset", ascending=False)
            .head(10)
        )
        dataframe_rf = sort_higher_publisher(data_df, high_publisher)

        filtered_df = dataframe_rf[(dataframe_rf["Center"] == center)].sort_values(
            "Number of Dataset", ascending=False
        )
        filtered_fair = dataframe_FAIR[(dataframe_FAIR["Center"] == center)]
        is_filtered = True
    else:
        high_publisher = (
            data_df[
                (data_df["Center"] == center)
                & (data_df["Research Field"] == research_field)
            ]
            .groupby("Publisher")
            .size()
            .reset_index(name="Number of Dataset")
            .sort_values("Number of Dataset", ascending=False)
            .head(10)
        )
        dataframe_rf = sort_higher_publisher(data_df, high_publisher)

        filtered_df = dataframe_rf[
            (dataframe_rf["Center"] == center)
            & (dataframe_rf["Research Field"] == research_field)
        ].sort_values("Number of Dataset", ascending=False)
        filtered_fair = dataframe_FAIR[
            (dataframe_FAIR["Center"] == center)
            & (dataframe_FAIR["Research Field"] == research_field)
        ]
        is_filtered = True

    try:
        # print(data_df)
        ###############################################################
        graph_repository_usage_chart = graph_repository_usage(filtered_df)
        ###############################################################
        graph_repository_usage_over_time_chart = graph_repository_usage_over_time(
            filtered_df
        )
        ###############################################################
        graph_average_fair_score_sunburst_chart = graph_average_fair_score_sunburst(
            filtered_fair
        )

        return (
            change_legend_according_viewport_size(
                graph_repository_usage_chart, viewport_size_width
            ),
            change_legend_according_viewport_size(
                graph_repository_usage_over_time_chart, viewport_size_width
            ),
            change_legend_according_viewport_size(
                graph_average_fair_score_sunburst_chart, viewport_size_width
            ),
            {"display": "block"} if is_filtered else {"display": "none"},
        )

    except (RuntimeError, TypeError, NameError, AttributeError):
        return None, None, None
