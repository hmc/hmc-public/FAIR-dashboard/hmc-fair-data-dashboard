# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0
"""
A collection of constants used by the fuji_scorer and datacite_info function.
"""
import os

FUJI_HOST = os.environ.get("FUJI_HOST", "localhost")
FUJI_PORT = os.environ.get("FUJI_PORT", "1071")
FUJI_PROTOCOL = os.environ.get("FUJI_PROTOCOL", "http")
FUJI_API = f"{FUJI_PROTOCOL}://{FUJI_HOST}:{FUJI_PORT}/fuji/api/v1/evaluate"
DATACITE_API = "https://api.datacite.org/dois/"
SCHOLIX_API = "http://api.scholexplorer.openaire.eu/v2/Links"


REQ_DICT = {"test_debug": True, "use_datacite": True}

HEADERS = {
    "accept": "application/json",
    "Authorization": "Basic bWFydmVsOndvbmRlcndvbWFu",
    "Content-Type": "application/json",
}

INITIAL_TIMEOUT = 300
EXTRA_TIMEOUT = 3
RETRY_WAITING_TIME = 3
MAX_RETRY_COUNTER = 2
