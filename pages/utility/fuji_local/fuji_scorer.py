# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0
import logging
import time

import pandas as pd
import requests
from requests.exceptions import ConnectionError as ConnectError
from requests.exceptions import ConnectTimeout
from requests.exceptions import ReadTimeout

from .fuji_conf import EXTRA_TIMEOUT
from .fuji_conf import FUJI_API
from .fuji_conf import HEADERS
from .fuji_conf import INITIAL_TIMEOUT
from .fuji_conf import MAX_RETRY_COUNTER
from .fuji_conf import REQ_DICT
from .fuji_conf import RETRY_WAITING_TIME


def get_fuji_score(
    data_pid,
    retry_count=0,
    extra_info=None,
):
    """
    Assess the F.A.I.R.-ness of a data-publication via the F-UJI server and return the resulting
    F-UJI scores in a dictionary.
    :param data_pid: the PID of a data-publication
    :param retry_count: Number of retries before
    :param extra_info: Extra information to join in final result
    :return: a dictionary of F-UJI metrics for the data-publication.


    """
    json = REQ_DICT | {"object_identifier": data_pid}
    logging.debug("FUJI scorer started for data_pid %s", data_pid)

    print(f"FUJI scorer started for data_pid {data_pid}")

    try:
        with requests.post(
            FUJI_API,
            json=json,
            headers=HEADERS,
            timeout=INITIAL_TIMEOUT + EXTRA_TIMEOUT * retry_count,
        ) as req:
            if req.status_code == 429:
                # print("Timeout")
                raise ReadTimeout

            logging.debug("FUJI done for data_pid %s", data_pid)

            return _select_fuji_metrics(req.json(), data_pid, extra_info)

    except (ReadTimeout, ConnectTimeout, ConnectError):
        if retry_count < MAX_RETRY_COUNTER:
            retry_count += 1
            logging.info("FUJI scorer timed out. Retry! Counter is now %s", retry_count)
            time.sleep(RETRY_WAITING_TIME * retry_count)

            return get_fuji_score(data_pid, retry_count, extra_info)

        logging.info(
            "FUJI scorer timed out after %s retries for DOI %s.",
            MAX_RETRY_COUNTER,
            data_pid,
        )

        return None


def _select_fuji_metrics(rs_json, pid, extra_info=None):
    """
    Select important information from F-UJI response.
    :param rs_json: the JSON got as a response from F-UJI
    :param pid: the PID of the data-publication
    :param extra_info: Extra information to join in final result
    :return:
    """

    # print(rs_json)
    if extra_info is None:
        extra_info = {}

    list_metric = {
        "pid": pid,
        "metric_specification": rs_json["metric_specification"],
        "timestamp": rs_json["end_timestamp"],
    }

    for score in rs_json["summary"]:
        for subscore in rs_json["summary"][score]:
            metric = score + "_" + subscore
            metric_score = rs_json["summary"][score][subscore]
            list_metric[metric] = float(metric_score)

    data_df = pd.DataFrame(
        columns=[
            "Title",
            "Publisher",
            "Publication Year",
            "Type",
            "PID",
            "metric_specification",
            "timestamp",
            "FAIR",
            "F",
            "F1",
            "F2",
            "F3",
            "F4",
            "A",
            "A1",
            "I",
            "I1",
            "I2",
            "I3",
            "R",
            "R1",
            "R1_1",
            "R1_2",
            "R1_3",
        ]
    )

    data_df.loc[0] = [
        extra_info["Title"],
        extra_info["Publisher"],
        extra_info["Publication Year"],
        extra_info["Type"],
        list_metric["pid"],
        list_metric["metric_specification"],
        list_metric["timestamp"],
        list_metric["score_percent_FAIR"],
        list_metric["score_percent_F"],
        list_metric["score_percent_F1"],
        list_metric["score_percent_F2"],
        list_metric["score_percent_F3"],
        list_metric["score_percent_F4"],
        list_metric["score_percent_A"],
        list_metric["score_percent_A1"],
        list_metric["score_percent_I"],
        list_metric["score_percent_I1"],
        list_metric["score_percent_I2"],
        list_metric["score_percent_I3"],
        list_metric["score_percent_R"],
        list_metric["score_percent_R1"],
        list_metric["score_percent_R1.1"],
        list_metric["score_percent_R1.2"],
        list_metric["score_percent_R1.3"],
    ]

    # print("FUJI scorer {}".format(data_df.head(1)))
    return data_df


if __name__ == "__main__":
    get_fuji_score("10.5517/ccdc.csd.cc27gdbg")
