# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0
import logging
import urllib.parse

import requests
from requests.exceptions import ConnectionError as ConnectError
from requests.exceptions import ConnectTimeout
from requests.exceptions import ReadTimeout

from .fuji_conf import SCHOLIX_API


def get_scholix_info(data_pid):
    try:
        with requests.get(
            SCHOLIX_API + "?sourcePid=" + urllib.parse.quote(data_pid.strip()),
            timeout=20,
        ) as req:
            if req.status_code == 429:
                raise ReadTimeout

            logging.debug("SCHOLIX API call done for data_pid %s", data_pid)

            if req.json()["totalLinks"] > 0:
                print("Yes, the result has data!")
                dataset = req.json()["result"][0]["source"]

                info = {
                    "Title": dataset["Title"],
                    "Publisher": _get_publishers(dataset)[0],
                    "Publication Year": _get_publication_year(dataset),
                    "Type": dataset["Type"],
                    "PID": data_pid,
                }
                print(info)
                return info
            print("No, the result has no data!")
            return None

    except (ReadTimeout, ConnectTimeout, ConnectError):
        logging.info("Datacite call timed out for DOI %s.", data_pid)

        return None


def _get_publishers(dataset):
    """
    Get and infer the publisher names.
    :param dataset: a dictionary representing a dataset
    :return: a list of publisher-names
    """
    publishers = [publisher["name"] for publisher in dataset["Publisher"]]
    id_schemes = [identifier["IDScheme"] for identifier in dataset["Identifier"]]

    if "pdb" in id_schemes:
        publishers.append("Protein Data Bank archive (PDB)")

    if "ena" in id_schemes:
        publishers.append("European Nucleotide Archive (ENA)")

    if "uniprot" in id_schemes:
        publishers.append("Universal Protein Knowledgebase (UniProt)")

    if {"genbank", "ncbi-p", "ncbi-n"} & set(id_schemes):
        publishers.append("National Library of Medicine (NLM)")

    return publishers


def _get_publication_year(dataset):
    """
    Get publication year.
    :param dataset: a dictionary representing a dataset
    :return: publication year
    """

    date = dataset["PublicationDate"]
    try:
        year = (str(date).split(" ", maxsplit=1)[0]).split("-", maxsplit=1)[0]
        year = year if year != "" else 0
        return 0 if date is None else int(year)
    except:
        print("Publication Year was not transformed, we have now 0 for it ")
        return 0
