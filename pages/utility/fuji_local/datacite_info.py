# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0
import logging

import requests
from requests.exceptions import ConnectionError as ConnectError
from requests.exceptions import ConnectTimeout
from requests.exceptions import ReadTimeout

from .fuji_conf import DATACITE_API


def get_datacite_info(data_pid):
    try:
        with requests.get(DATACITE_API + data_pid, timeout=20) as req:
            if req.status_code == 429:
                raise ReadTimeout

            logging.debug("Datacite call done for data_pid %s", data_pid)

            if "data" in req.json():
                data = req.json()["data"]["attributes"]
                # print(data['titles'][0]['title'])

                info = {
                    "Title": data["titles"][0]["title"],
                    "Publisher": data["publisher"],
                    "Publication Year": data["publicationYear"],
                    "Type": data["types"],
                    "PID": data["doi"],
                }

                return info

            return None

    except (ReadTimeout, ConnectTimeout, ConnectError):
        logging.info("Datacite call timed out for DOI %s.", data_pid)

        return None
