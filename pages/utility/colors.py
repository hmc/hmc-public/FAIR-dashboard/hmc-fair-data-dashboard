# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0
# Official color codes of HMC
colors_hmc = {
    "Aeronautics": "#50c8aa",
    "Earth and Environment": "#326469",
    "Energy": "#ffd228",
    "FAIR Data Commons": "#9bc53c",
    "Health": "#d23264",
    "HMC Office": "#2eb2fa",
    "HMC Projects": "#5e3b99",
    "Matter": "#f0781e",
    "Information": "#a0235a",
    "Programme-independent research": "#005aa0",
}
