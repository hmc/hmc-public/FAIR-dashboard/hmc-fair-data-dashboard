# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0
import re
import textwrap

list_fair = [
    "FAIR",
    "F",
    "F1",
    "F2",
    "F3",
    "F4",
    "A",
    "A1",
    "I",
    "I1",
    "I2",
    "I3",
    "R",
    "R1",
    "R1_1",
    "R1_2",
    "R1_3",
]

list_fair_total = [
    "FAIR",
    "F",
    "F1",
    "F2",
    "F3",
    "F4",
    "A",
    "A1",
    "I",
    "I1",
    "I2",
    "I3",
    "R",
    "R1",
    "R1_1",
    "R1_2",
    "R1_3",
    "Earned A",
    "Earned A1",
    "Earned F",
    "Earned F1",
    "Earned F2",
    "Earned F3",
    "Earned F4",
    "Earned FAIR",
    "Earned I",
    "Earned I1",
    "Earned I2",
    "Earned I3",
    "Earned R",
    "Earned R1",
    "Earned R1_1",
    "Earned R1_2",
    "Earned R1_3",
    "Total A",
    "Total A1",
    "Total F",
    "Total F1",
    "Total F2",
    "Total F3",
    "Total F4",
    "Total FAIR",
    "Total I",
    "Total I1",
    "Total I2",
    "Total I3",
    "Total R",
    "Total R1",
    "Total R1_1",
    "Total R1_2",
    "Total R1_3",
]


###############################################################
def is_valid_doi(input_text):
    return bool(re.search(r"10[.]\d{3,}(?:[.]\d+)*/\S+", input_text))


###############################################################
def get_last_update_date(last_updated_info_df):
    last_updated = last_updated_info_df.sum()
    if last_updated == 0:
        return 0
    return last_updated.strftime("%Y/%m/%d")


###############################################################


def get_data_fair_by_publisher(dataframe):
    """Returns the FAIR scores per Indicator, grouped by Publisher."""
    dataframe = dataframe.groupby("Publisher")[list_fair].mean().reset_index()
    return dataframe


def get_fair_scores(dataframe):
    """Returns the FAIR scores per Indicator, grouped by Publisher and Publication Year."""
    dataframe = (
        dataframe.groupby(["Publisher", "Publication Year"])[list_fair]
        .mean()
        .reset_index()
    )
    return dataframe


def get_fair_scores_by_centers_research_field(dataframe):
    """Returns the FAIR scores per Indicator, grouped by Publisher, Center, Research Field, and Publication Year."""

    dataframe = (
        dataframe.groupby(
            [
                "Publisher",
                "Center",
                "Research Field",
                "Publication Year",
            ]
        )[list_fair_total]
        .mean()
        .reset_index()
    )
    return dataframe


def get_count_dataset(dataframe):
    """Get number of data publications per publishers, publication year and Relationship Type"""
    dataframe = (
        dataframe.groupby(["Publisher", "Publication Year", "Relationship Type"])
        .size()
        .reset_index(name="Number of Dataset")
    )
    dataframe = dataframe.sort_values("Publication Year").reset_index(drop=True)
    return dataframe


def get_count_dataset_is_supplemented_by_rf(dataframe):
    """Get number of data publications per publishers, publication year and Research Field"""

    dataframe = (
        dataframe.groupby(["Publisher", "Publication Year", "Research Field"])
        .size()
        .reset_index(name="Number of Dataset")
    )
    dataframe = dataframe.sort_values("Publication Year").reset_index(drop=True)
    return dataframe


####################################################
####################################################
####################################################
def get_count_dataset_by_rf_repositories_center(dataframe):
    """Get number of data publications per publishers, Center, publication year and Research Field"""
    dataframe = (
        dataframe.groupby(
            [
                "Publisher",
                "Center",
                "Publication Year",
                "Research Field",
            ]
        )
        .size()
        .reset_index(name="Number of Dataset")
    )

    return dataframe


def get_sum_dataset_by_rf_repositories(dataframe):
    """Get sum of data publications per publishers and Research Field"""
    dataframe = dataframe.groupby(["Publisher", "Research Field"]).sum().reset_index()
    return dataframe


def get_sum_dataset_by_rf_repositories_year(dataframe):
    """Get sum of data publications per publishers and Publication Year"""
    dataframe = dataframe.groupby(["Publisher", "Publication Year"]).sum().reset_index()
    return dataframe


def get_sum_dataset_by_rf_year(dataframe):
    """Get sum of data publications per Publication Year and Research Field"""
    dataframe = (
        dataframe.groupby(["Publication Year", "Research Field"]).sum().reset_index()
    )
    return dataframe


def get_sum_dataset_by_center_year(dataframe):
    """Get sum of data publications per Center and Publication Year"""
    dataframe = dataframe.groupby(["Center", "Publication Year"]).sum().reset_index()
    return dataframe


#######################################################
#######################################################
#######################################################
def get_count_dataset_is_supplemented_by_center(dataframe):
    """Get number of data publications per publishers, Publication Year and Center"""
    dataframe = (
        dataframe.groupby(["Publisher", "Publication Year", "Center"])
        .size()
        .reset_index(name="Number of Dataset")
    )
    dataframe = dataframe.sort_values("Publication Year").reset_index(drop=True)
    return dataframe


def get_avg_fair_group(dataframe):
    """Get count of FAIR data publications per publication year and Publisher"""
    df_avg = (
        dataframe.groupby(["Publisher", "Publication Year"])["FAIR"]
        .mean()
        .reset_index(name="Avg FAIR")
    )
    return df_avg


def get_count_pid_types(dataframe):
    """Get count of data publications per publication year and Identifier Type"""
    dataframe = (
        dataframe.groupby(
            [
                "Publisher",
                "Publication Year",
                "Identifier Type",
            ]
        )
        .size()
        .reset_index(name="Number of Dataset")
    )
    dataframe = dataframe.sort_values("Publication Year").reset_index(drop=True)
    return dataframe


###########################################################################
#######################literature##########################################
###########################################################################
def get_count_literature(dataframe):
    """Get count of publications per Center, publication year and research field"""
    dataframe = (
        dataframe.groupby(["Center", "Publication Year", "Research Field"])
        .size()
        .reset_index(name="Number of Literature")
    )
    dataframe = dataframe.sort_values("Publication Year").reset_index(drop=True)
    return dataframe


def get_count_pid_literature_types(dataframe):
    """Get count of publications per Center, publication year and Identifier Type"""
    dataframe = (
        dataframe.groupby(
            [
                "Center",
                "Publication Year",
                "Identifier Type",
            ]
        )
        .size()
        .reset_index(name="Number of Literature")
    )
    dataframe = dataframe.sort_values("Publication Year").reset_index(drop=True)
    return dataframe


def get_count_literature_inhouse(dataframe):
    """Get count of publications per Center, publication year and In House Research"""
    dataframe = (
        dataframe.groupby(["Center", "Publication Year", "In House Research"])
        .size()
        .reset_index(name="Number of Literature")
    )
    dataframe = dataframe.sort_values("Publication Year").reset_index(drop=True)
    return dataframe


def get_count_literature_instrument(dataframe):
    """Get count of publications per Center, publication year and Instrument"""
    dataframe = (
        dataframe.groupby(["Center", "Publication Year", "Instrument"])
        .size()
        .reset_index(name="Number of Literature")
    )
    dataframe = dataframe.sort_values("Publication Year").reset_index(drop=True)
    return dataframe


def get_count_literature_large_scale_facility(dataframe):
    """Get count of publications per Center, publication year and Large Scale Facility"""
    dataframe = (
        dataframe.groupby(["Center", "Publication Year", "Large Scale Facility"])
        .size()
        .reset_index(name="Number of Literature")
    )
    dataframe = dataframe.sort_values("Publication Year").reset_index(drop=True)
    return dataframe


def get_formatted_df_literatures(dataframe):
    """Get formatting the literatures dataframe"""
    dataframe["In House Research"] = dataframe["In House Research"].replace(
        -1, "Unknown"
    )
    dataframe["In House Research"] = dataframe["In House Research"].replace(1, "Yes")
    dataframe["In House Research"] = dataframe["In House Research"].replace(0, "No")

    def used_one_or_more(value):
        return "No" if value is None else "Yes"

    dataframe["Instrument"] = dataframe["Instrument"].apply(used_one_or_more)
    dataframe["Large Scale Facility"] = dataframe["Large Scale Facility"].apply(
        used_one_or_more
    )

    # df = df[df['Publication Year'] >= 2000]
    return dataframe


#############################FAIR PER DOI############################################
def get_fair_scores_per_doi(dataframe):
    """Returns the FAIR scores per Indicator, grouped by Publisher and Publication Year."""
    dataframe = dataframe[
        ["Publisher", "Publication Year", "Title", "Type"] + list_fair
    ]
    return dataframe


def get_data_fair_by_publisher_per_doi(dataframe):
    """Returns the FAIR scores per Indicator, grouped by Publisher."""
    dataframe = (
        dataframe.groupby(["Publisher", "Publication Year", "Title", "Type"])[list_fair]
        .mean()
        .reset_index()
    )
    return dataframe


########################################################################
def get_count_by_year(dataframe, name):
    """Get count of publications per Publication Type, and publication year"""
    dataframe = dataframe[dataframe["Publication Year"] >= 2000]
    dataframe = (
        dataframe.groupby(
            [
                "Publication Type",
                "Publication Year",
            ]
        )
        .size()
        .reset_index(name=name)
    )
    dataframe = dataframe.sort_values("Publication Year").reset_index(drop=True)
    return dataframe


###########################################################################
def warp_graph_title_according_viewport_size(
    graph, title, viewport_size_width=599, width=50
):
    if viewport_size_width < 600:
        graph.update_layout(
            title={
                "text": "<br>".join(textwrap.wrap(title, width=width)),
                "xanchor": "center",
                "yanchor": "top",
            }
        )
    return graph


def change_legend_according_viewport_size(
    graph,
    viewport_size_width,
):
    if viewport_size_width < 600:
        graph.update_layout(
            legend={"orientation": "h", "y": -0.2, "x": 0},
        )
    return graph


def update_graph_layout(
    graph,
    title,
    yaxis_title,
    xaxis_title,
    legend_mode="h",
):
    """Updating the graph layout"""
    graph.update_layout(
        title=title,
        yaxis_title=yaxis_title,
        xaxis_title=xaxis_title,
        font_family="halvar",
        margin={"b": 10},
        yaxis={"automargin": True},
        autosize=True,
        height=600,
        transition_duration=500,
        title_x=0.5,
        title_y=0.9,
        hoverlabel={"font_family": "halvar"},
        hovermode="x",
        legend={"orientation": "h", "y": -0.2, "x": 0} if legend_mode == "h" else None,
    )


def update_graph_layout_min(graph):
    """Updating the graph layout"""
    graph.update_layout(
        transition_duration=500,
        title_x=0.5,
        title_y=0.9,
        font_family="halvar",
        yaxis={"autorange": True, "automargin": True},
        xaxis={"autorange": True, "automargin": True},
        margin={"b": 10},
        autosize=True,
        height=600,
        hovermode="x unified",
        hoverlabel={"font_family": "halvar"},
    )


def update_graph_layout_min_vertical(graph):
    """Updating the graph layout vertical"""
    graph.update_layout(
        transition_duration=500,
        title_x=0.5,
        title_y=0.9,
        font_family="halvar",
        xaxis={"autorange": True, "automargin": True},
        yaxis={
            "autorange": True,
            "automargin": True,
            "showticklabels": True,
        },
        margin={"b": 10},
        autosize=True,
        height=600,
        hovermode="y unified",
        hoverlabel={"font_family": "halvar"},
    )


def update_graph_layout_my_data_fair(graph, hover_indicator, xaxis_title, yaxis_title):
    """Updating the graph layout, axis, and hover text of My Data Fair Part"""
    hover_template = (
        "<b>%{hovertext} </b><br /><br /><b>"
        + hover_indicator
        + ": </b> %{x:.1f} % <extra></extra>"
    )
    graph.update_traces(hovertemplate=hover_template)
    # overwrite tick labels
    graph.update_layout(
        xaxis={"title": xaxis_title},
        yaxis={"showticklabels": False, "title": yaxis_title},
        legend_traceorder="grouped",
        bargap=0.3,
    )


########################################################


def update_graph_layout_no_filter_match(graph, text):
    """Updating the graph layout when no filter match and show text instead"""
    graph.update_layout(
        xaxis={"visible": False},
        yaxis={"visible": False},
        annotations=[
            {
                "text": text,
                "xref": "paper",
                "yref": "paper",
                "font_size": 20,
            }
        ],
    )
