# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0
import os

import pandas as pd
import pymysql
from dotenv import find_dotenv
from dotenv import load_dotenv

load_dotenv(find_dotenv())

DB_CONTAINER_NAME_OR_ADDRESS = os.environ.get("DB_CONTAINER_NAME_OR_ADDRESS")
DB_NAME = os.environ.get("DB_NAME")
DB_USER = os.environ.get("DB_USER")
DB_PASSWORD = os.environ.get("DB_PASSWORD")


def connection():
    return pymysql.connect(
        host=DB_CONTAINER_NAME_OR_ADDRESS,
        user=DB_USER,
        password=DB_PASSWORD,
        database=DB_NAME,
    )


MAX_YEAR = int(os.environ.get("MAX_YEAR", "2024"))


# publication table
SELECT_LITERATURE_PUBLICATION_COUNT_QUERY = """SELECT p.publication_year as 'Publication Year',
p.`type` as 'Publication Type', COUNT(DISTINCT p.id) as 'Total Number of Literatures'
FROM publication AS p
INNER JOIN center_has_publication AS pc ON p.id = pc.publication_id
INNER JOIN center AS c ON pc.center_id = c.id
WHERE p.`type` = 'Literature' AND p.publication_year >= 2000 AND p.publication_year <= %s
GROUP BY p.publication_year
ORDER BY p.publication_year
"""

SELECT_DATASET_PUBLICATION_COUNT_QUERY = """SELECT p.publication_year as 'Publication Year',
 p.`type` as 'Publication Type', COUNT(DISTINCT p.id) as 'Total Number of Datasets'
FROM publication AS p
INNER JOIN reference AS r ON p.id = r.reference_to_id
INNER JOIN publication AS pr ON pr.id = r.reference_id
INNER JOIN center_has_publication AS pc ON pr.id = pc.publication_id
INNER JOIN center AS c ON pc.center_id = c.id
WHERE p.`type` = 'Dataset' AND  r.`sub_type` = 'IsSupplementedBy'  AND  p.publication_year >= 2000
AND p.publication_year <= %s
GROUP BY p.publication_year
ORDER BY p.publication_year"""

SELECT_DATASET_PUBLICATION_BY_DOI_QUERY = """ SELECT p.id as 'ID', p.title as Title,
 p.journal as Journal, p.publisher_deprecated as Publisher,
p.publication_year as 'Publication Year', p.`type` as 'Type',
i.identifier_id as 'Identifier ID', i.identifier_type as 'Identifier Type',
i.identifier_url as 'Identifier URL', r.`sub_type` as 'Relationship Type',
f.score_percent_FAIR as FAIR, f.score_percent_F as F,
f.score_percent_F1 as F1, f.score_percent_F2 as F2,
f.score_percent_F3 as F3, f.score_percent_F4 as F4,
f.score_percent_A as A, f.score_percent_A1 as A1,
f.score_percent_I as I, f.score_percent_I1 as I1,
f.score_percent_I2 as I2,
f.score_percent_I3 as I3, f.score_percent_R as R,
f.score_percent_R1 as R1, f.`score_percent_R1.1` as R1_1,
f.`score_percent_R1.2` as R1_2, f.`score_percent_R1.3` as R1_3
FROM publication AS p
INNER JOIN fuji_score AS f ON p.id = f.publication_id
INNER JOIN publication_identifier AS i ON p.id = i.publication_id
INNER JOIN reference AS r ON p.id = r.reference_to_id
WHERE i.identifier_id = %s"""

SELECT_CENTERS_QUERY = """SELECT id as ID, name as Name, description as Description  FROM center ORDER BY name ASC"""


SELECT_LAST_UPDATED_INFO_QUERY = """SELECT MIN(last_updated) as min_last_updated_info, MAX(last_updated)
as max_last_updated_info FROM publication"""

SELECT_RESEARCH_FIELDS_QUERY = """SELECT id as ID, name as Name, description as Description
 FROM research_field ORDER BY name ASC"""

SELECT_DATASET_PUBLISHERS_QUERY = """SELECT distinct publisher_deprecated as Publisher FROM publication AS p
INNER JOIN reference AS r ON p.id = r.reference_to_id
 WHERE p.`type` = 'Dataset' AND  r.`sub_type` = 'IsSupplementedBy'
 ORDER BY publisher ASC"""

SELECT_DATASET_PUBLICATION_IS_SUPPLEMENTED_BY_QUERY_VIEW = """
CREATE OR REPLACE VIEW calculated_view AS SELECT
DISTINCT p.id as 'ID',  p.publisher_deprecated as Publisher,
p.publication_year as 'Publication Year', p.`type` as 'Publication Type',
r.`sub_type` as 'Relationship Type',
rf.name as 'Research Field', c.name as Center,
f.score_percent_FAIR as FAIR, f.score_percent_F as F,
f.score_percent_F1 as F1, f.score_percent_F2 as F2,
f.score_percent_F3 as F3, f.score_percent_F4 as F4,
f.score_percent_A as A, f.score_percent_A1 as A1,
f.score_percent_I as I, f.score_percent_I1 as I1, f.score_percent_I2 as I2,
f.score_percent_I3 as I3, f.score_percent_R as R,
f.score_percent_R1 as R1, f.`score_percent_R1.1` as R1_1,
f.`score_percent_R1.2` as R1_2, f.`score_percent_R1.3` as R1_3,
f.score_earned_A  as 'Earned A', f.score_earned_A1 as 'Earned A1',
f.score_earned_F as 'Earned F', f.score_earned_F1 as 'Earned F1',
f.score_earned_F2 as 'Earned F2', f.score_earned_F3 as 'Earned F3',
f.score_earned_F4 as 'Earned F4', f.score_earned_FAIR as 'Earned FAIR',
f.score_earned_I as 'Earned I', f.score_earned_I1 as 'Earned I1', f.score_earned_I2 as 'Earned I2',
f.score_earned_I3 as 'Earned I3', f.score_earned_R as 'Earned R',
f.score_earned_R1 as 'Earned R1', f.`score_earned_R1.1` as 'Earned R1_1',
f.`score_earned_R1.2` as 'Earned R1_2', f.`score_earned_R1.3` as 'Earned R1_3',
f.score_total_A as 'Total A', f.score_total_A1 as 'Total A1',
f.score_total_F as 'Total F', f.score_total_F1 as 'Total F1',
f.score_total_F2 as 'Total F2', f.score_total_F3 as 'Total F3',
f.score_total_F4 as 'Total F4', f.score_total_FAIR as 'Total FAIR',
f.score_total_I as 'Total I', f.score_total_I1 as 'Total I1', f.score_total_I2 as 'Total I2',
f.score_total_I3 as 'Total I3', f.score_total_R as 'Total R',
f.score_total_R1 as 'Total R1', f.`score_total_R1.1` as 'Total R1_1',
f.`score_total_R1.2` as 'Total R1_2', f.`score_total_R1.3` as 'Total R1_3'
FROM publication AS p
INNER JOIN fuji_score AS f ON p.id = f.publication_id
INNER JOIN publication_identifier AS i ON p.id = i.publication_id
INNER JOIN reference AS r ON p.id = r.reference_to_id
INNER JOIN publication AS pr ON pr.id = r.reference_id
INNER JOIN research_field_has_publication AS rp ON pr.id = rp.publication_id
INNER JOIN research_field AS rf ON rp.research_field_id = rf.id
INNER JOIN center_has_publication AS pc ON pr.id = pc.publication_id
INNER JOIN center AS c ON pc.center_id = c.id
WHERE  p.`type` = 'Dataset'  AND  p.publication_year >= 2000 AND p.publication_year <= %s AND r.`sub_type` = 'IsSupplementedBy';
"""

SELECT_DATASET_PUBLICATION_IS_SUPPLEMENTED_BY_QUERY = (
    """SELECT * FROM calculated_view"""
)


def initialized_view(
    query, args=None, success_message="CALCULATED VIEW Successfully initialized!"
):
    conn = connection()
    cursor = conn.cursor()

    cursor.execute(query, args)

    try:
        conn.commit()
        cursor.close()
        print(success_message)

    except pymysql.Error as err:
        print(f"could not close connection error pymysql {err.args[0]}: {err.args[1]}")
        conn.rollback()
        cursor.close()


def get_dataframe(
    query, args=None, success_message="Data Selection Successfully Done!"
):
    conn = connection()
    cursor = conn.cursor()
    cursor.execute(query, args)
    data = cursor.fetchall()
    dataframe = pd.DataFrame(data, columns=[i[0] for i in cursor.description])
    try:
        conn.commit()
        cursor.close()
        print(success_message)
        return dataframe

    except pymysql.Error as err:
        print(f"could not close connection error pymysql {err.args[0]}: {err.args[1]}")
        conn.rollback()
        cursor.close()
        return None


def get_dataframe_per_doi(doi):
    conn = connection()
    cursor = conn.cursor()
    cursor.execute(SELECT_DATASET_PUBLICATION_BY_DOI_QUERY, doi)
    data = cursor.fetchall()
    dataframe = pd.DataFrame(data, columns=[i[0] for i in cursor.description])
    try:
        conn.commit()
        cursor.close()
        if dataframe.empty:
            print("DOI not found in database!")
            return False, dataframe

        print("DOI found in database!")
        return True, dataframe
    except pymysql.Error as err:
        print(f"could not close connection error pymysql {err.args[0]}: {err.args[1]}")
        conn.rollback()
        cursor.close()
        return None


# Main Database Queries Operations
initialized_view(
    SELECT_DATASET_PUBLICATION_IS_SUPPLEMENTED_BY_QUERY_VIEW, args=[MAX_YEAR]
)

literature_count_df = get_dataframe(
    SELECT_LITERATURE_PUBLICATION_COUNT_QUERY,
    args=[MAX_YEAR],
    success_message="LITERATURE PUBLICATION COUNT queried successfully",
)
data_count_df = get_dataframe(
    SELECT_DATASET_PUBLICATION_COUNT_QUERY,
    args=[MAX_YEAR],
    success_message="DATASET PUBLICATION COUNT queried successfully",
)
data_is_supplemented_by_df = get_dataframe(
    SELECT_DATASET_PUBLICATION_IS_SUPPLEMENTED_BY_QUERY,
    success_message="IS_SUPPLEMENTED_BY Data Publications queried successfully",
)
center_df = get_dataframe(
    SELECT_CENTERS_QUERY, success_message="CENTERS queried successfully"
)
last_updated_info_df = get_dataframe(
    SELECT_LAST_UPDATED_INFO_QUERY,
    success_message="LAST UPDATED INFO queried successfully",
)

publisher_df = get_dataframe(
    SELECT_DATASET_PUBLISHERS_QUERY, success_message="PUBLISHERS queried successfully"
)
research_field_df = get_dataframe(
    SELECT_RESEARCH_FIELDS_QUERY, success_message="RESEARCH FIELDS queried successfully"
)
