# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0
import json
import os

import dash
import i18n
from dash import callback
from dash import ctx
from dash import dcc
from dash import html
from dash import Input
from dash import Output
from dash import State
from dash.exceptions import PreventUpdate

from .partials.banner import banner
from .partials.footer import footer
from .partials.my_data.bottom_info_box import bottom_info_box
from .partials.my_data.charts import a_chart
from .partials.my_data.charts import f_chart
from .partials.my_data.charts import get_my_data
from .partials.my_data.charts import i_chart
from .partials.my_data.charts import r_chart
from .partials.my_data.filter_options import filter_options
from .partials.navbar import navbar


dash.register_page(
    __name__, path_template="/<lang>/assess-my-data", title="Assess My Data"
)


def layout(lang="en"):
    """Complete Assess My Data page layout with Navbar, Banner, Page Content and Footer

    Parameters
    ----------
    lang : string
        language part from URL variable path template of the Dash App

    Returns
    -------
    layout for Assess My Data page
    """
    i18n.set("locale", lang)
    i18n.set("fallback", "en")
    i18n.load_path.append(os.path.abspath("translations"))

    return html.Div(
        [
            # Navbar and Banner
            navbar(),
            banner(),
            # Assess My Data page self-content
            html.Div(
                [
                    html.H2(i18n.t("my_data.title"), className="fw-bold pt-5 pb-3"),
                    # top info box
                    html.Div(
                        dcc.Markdown(
                            i18n.t("my_data.infoContent"), link_target="_blank"
                        ),
                        className="card p-5 info-box",
                    ),
                    filter_options(),
                    html.Div("", id="my_data"),
                    # dcc.Store stores the intermediate value
                    dcc.Store(id="intermediate_value"),
                    # read on info box
                    html.Div(
                        dcc.Markdown(i18n.t("my_data.readOn")),
                        className="card p-3 info-box",
                    ),
                    bottom_info_box(),
                ],
                className="container-xl",
            ),
            # Footer part
            footer(),
        ],
    )


@callback(
    Output("intermediate_value", "data", allow_duplicate=True),
    Output("my_data", "children", allow_duplicate=True),
    Input("search_btn", "n_clicks"),
    State("input_on_submit", "value"),
    prevent_initial_call=True,
)
def get_clean_data_per_pid(_btn, value):
    if ctx.triggered_id == "search_btn":
        # print(ctx.triggered_id)
        return get_my_data(value, False)
    raise PreventUpdate


@callback(
    Output("graph_details_graph", "children"),
    Input("intermediate_value", "data"),
    Input("assess_my_data_fair_mode", "value"),
)
def update_details_graph(stored_data, fair_mode):
    if stored_data is not None:
        dataset = json.loads(stored_data)
        if fair_mode == "f":
            return f_chart(dataset)
        if fair_mode == "a":
            return a_chart(dataset)
        if fair_mode == "i":
            return i_chart(dataset)
        if fair_mode == "r":
            return r_chart(dataset)

    return None


@callback(
    Output("intermediate_value", "data", allow_duplicate=True),
    Output("my_data", "children", allow_duplicate=True),
    Input("go_live_btn", "n_clicks"),
    State("input_on_submit", "value"),
    prevent_initial_call=True,
)
def process_live_assessment(n_clicks, value):
    if ctx.triggered_id == "go_live_btn":
        if n_clicks >= 1:
            return get_my_data(value, True)
    raise PreventUpdate
