# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0
import os

import dash
import i18n
from dash import callback
from dash import dcc
from dash import html
from dash import Input
from dash import Output

from .partials.banner import banner
from .partials.fair_by_repository.bottom_info_box import bottom_info_box
from .partials.fair_by_repository.charts import graph_average_fair_score_sunburst
from .partials.fair_by_repository.charts import graph_centers_in_repository
from .partials.fair_by_repository.charts import graph_dataset_publication_fair_line
from .partials.fair_by_repository.charts import graph_research_field_in_repository
from .partials.fair_by_repository.filter_options import filter_options
from .partials.fair_by_repository.html_template import charts
from .partials.footer import footer
from .partials.navbar import navbar
from .utility.db import data_is_supplemented_by_df as data_df
from .utility.helper import change_legend_according_viewport_size
from .utility.helper import get_count_dataset_by_rf_repositories_center
from .utility.helper import get_fair_scores_by_centers_research_field

dash.register_page(
    __name__, path_template="/<lang>/fair-by-repository", title="Repositories"
)
data_df = data_df[data_df["Publication Year"] >= 2000]
dataframe_rf = get_count_dataset_by_rf_repositories_center(data_df)
dataframe_fair = get_fair_scores_by_centers_research_field(data_df)


def layout(lang="en"):
    """Complete Repositories page layout with Navbar, Banner, Page Content and Footer

    Parameters
    ----------
    lang : string
        language part from URL variable path template of the Dash App

    Returns
    -------
    layout for Repositories page
    """
    i18n.set("locale", lang)
    i18n.set("fallback", "en")
    i18n.load_path.append(os.path.abspath("translations"))

    return html.Div(
        [
            # Navbar and Banner
            navbar(),
            banner(),
            # Repositories page self-content
            html.Div(
                [
                    html.H2(i18n.t("repo.title"), className="fw-bold pt-5 pb-3"),
                    # top info box
                    html.Div(
                        dcc.Markdown(i18n.t("repo.infoContent")),
                        className="card p-5 info-box",
                    ),
                    filter_options(),
                    charts(),
                    # read on info box
                    html.Div(
                        dcc.Markdown(i18n.t("repo.readOn")),
                        className="card p-3 info-box",
                    ),
                    bottom_info_box(),
                ],
                className="container-xl",
            ),
            # Footer part
            footer(),
        ],
    )


@callback(
    Output("graph_research_field_in_repository", "figure"),
    Output("graph_centers_in_repository", "figure"),
    Output("graph_average_FAIR_score_sunburst_in_repository", "figure"),
    Output("filter_msg_fair_by_repository_page", component_property="style"),
    Input("repository_filter", "value"),
    Input("viewport_container", "data"),
)
def update_graph_figures(repository, viewport_size):
    viewport_size_width = viewport_size["width"]
    is_filtered = False
    if repository == "all":
        filtered_df = dataframe_rf.sort_values("Number of Dataset", ascending=False)
        filtered_fair = dataframe_fair
        is_filtered = False
    else:
        filtered_df = dataframe_rf[
            (dataframe_rf["Publisher"] == repository)
        ].sort_values("Number of Dataset", ascending=False)
        filtered_fair = dataframe_fair[(dataframe_fair["Publisher"] == repository)]
        is_filtered = True

    try:
        ###################################################
        graph_research_field_in_repository_chart = graph_research_field_in_repository(
            filtered_df
        )
        graph_centers_in_repository_chart = graph_centers_in_repository(filtered_df)
        graph_average_fair_score_sunburst_chart = graph_average_fair_score_sunburst(
            filtered_fair
        )
        ############################################################
        return (
            change_legend_according_viewport_size(
                graph_research_field_in_repository_chart, viewport_size_width
            ),
            change_legend_according_viewport_size(
                graph_centers_in_repository_chart, viewport_size_width
            ),
            change_legend_according_viewport_size(
                graph_average_fair_score_sunburst_chart, viewport_size_width
            ),
            {"display": "block"} if is_filtered else {"display": "none"},
        )

    except (RuntimeError, TypeError, NameError, AttributeError):
        return None, None, None


@callback(
    Output("graph_average_FAIR_score_line_in_repository", "figure"),
    Input("repository_filter", "value"),
    Input("fair_mode", "value"),
    Input("viewport_container", "data"),
)
def update_graph_average_fair_score_figure(repository, fair_mode, viewport_size):
    viewport_size_width = viewport_size["width"]
    if repository == "all":
        filtered_fair = dataframe_fair

    else:
        filtered_fair = dataframe_fair[(dataframe_fair["Publisher"] == repository)]

    ############################################################
    graph_dataset_publication_fair_line_chart = graph_dataset_publication_fair_line(
        filtered_fair, fair_mode
    )

    return change_legend_according_viewport_size(
        graph_dataset_publication_fair_line_chart, viewport_size_width
    )
