# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0
import os

import dash
import i18n
from dash import dcc
from dash import html

from .partials.about.bottom_info_box import bottom_info_box
from .partials.banner import banner
from .partials.footer import footer
from .partials.navbar import navbar

dash.register_page(__name__, path_template="/<lang>/about", title="About")


def layout(lang="en"):
    """Complete About page layout with Navbar, Banner, Page Content and Footer

    Parameters
    ----------
    lang : string
        language part from URL variable path template of the Dash App

    Returns
    -------
    layout for About page
    """
    i18n.set("locale", lang)
    i18n.set("fallback", "en")
    i18n.load_path.append(os.path.abspath("translations"))

    return html.Div(
        [
            # Navbar and Banner
            navbar(),
            banner(),
            # About page self-content
            html.Div(
                [
                    html.H2(i18n.t("about.title"), className="fw-bold pt-5 pb-3"),
                    # top info box
                    html.Div(
                        dcc.Markdown(i18n.t("about.infoContent")),
                        className="card p-5 info-box",
                    ),
                    html.Div(
                        dcc.Markdown(i18n.t("about.infoBoxTechnicalDocumentation")),
                        className="card p-5 info-box",
                    ),
                    html.Div(
                        dcc.Markdown(i18n.t("about.infoBoxHowToCite")),
                        className="card p-5 info-box",
                    ),
                    html.Div(
                        dcc.Markdown(i18n.t("about.infoContent2")),
                        className="card p-5 info-box",
                    ),
                    html.Div(
                        [
                            html.Img(
                                src="/assets/images/about.png",
                                className="img-fluid pb-5",
                            ),
                            html.Div(
                                dcc.Markdown(i18n.t("about.content")),
                                style={"padding": "2rem"},
                                className="content",
                            ),
                        ]
                    ),
                    html.Div(
                        dcc.Markdown(i18n.t("about.disclaimer")),
                        className="card p-5 info-box",
                    ),
                    bottom_info_box(),
                ],
                className="container-xl",
            ),
            # Footer part
            footer(),
        ],
    )
