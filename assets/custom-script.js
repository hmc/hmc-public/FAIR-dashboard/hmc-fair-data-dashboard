// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
//
// SPDX-License-Identifier: Apache-2.0

// When the user scrolls down 80px from the top of the document, resize the navbar's padding and the logo's font size
window.onscroll = function () {
    scrollFunction()
};

function scrollFunction() {
    if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
        document.getElementById("logo").style.height = "30px";
        document.getElementById("languages").style.marginTop = "-40px"


    } else {
        document.getElementById("logo").style.height = "45px";
        document.getElementById("languages").style.marginTop = "0px"
    }
}
