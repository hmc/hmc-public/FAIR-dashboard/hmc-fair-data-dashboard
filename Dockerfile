# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0

FROM python:3.10
EXPOSE 8050
COPY . ./src

WORKDIR /src
RUN pip install --upgrade pip
RUN pip install -e .

CMD ["gunicorn", "--config=gunicorn.conf.py", "app:server"]
