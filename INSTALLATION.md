<!--
SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany

SPDX-License-Identifier: CC-BY-4.0
-->


# HMC FAIR Data Dashboard

<img src="https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/hmc-fair-data-dashboard/-/raw/development/dashboard_logo.png" alt="EM Glossary Group Logo" width="200"/>


## Complete Documentation

The complete documentation is available on "[Documentation](https://documentation-hmc-hmc-public-fair-dashboard-f5717fe4ead77cf07ba.pages.hzdr.de/)" (**The documentation URL is temporary, and will change soon to a clean URL**).

### Quik Installation instructions

We have two options of installation of the ``HMC FAIR Data Dashboard``, which are:

- Python Virtual Environments
- Using Docker Containers

#### Python Virtual Environments

##### Install Dependencies (Linux)

  ```bash
 sudo apt install python3-pip python3-venv git
  ```

##### Cloning the Repository

```bash
 $ git clone git@codebase.helmholtz.cloud:hmc/hmc-public/FAIR-dashboard/hmc-fair-data-dashboard.git
```

##### Creating Virtual Environments

```bash
  $ cd hmc-fair-data-dashboard    # change to app directory
  $ python3 -m venv env        # creates a virtual environment in directory 'env'
  $ source env/bin/activate    # enables the virtual environment
```

##### Installing Requirements and Packages

```bash
  (.env) $ pip install -e .
```

##### Adding the ``.env`` file
Copy the `.env.example` and then fill it with your desire variable
```bash
  (.env) $ cp .env.example .env
```


##### Running the project locally

```bash
  (.env) $ python app.py
```

Note:

- Dash is running on ``http://127.0.0.1:8050/``.

- You also can change ``app.run_server()``
  to ``app.run_server(debug=True)`` in ``app.py`` file for more development process, if you need.

#####  Running in production mode

If you want to install as the same steps in your production server, you can, just run via `run.sh` file.

```bash
(.env) $ ./run.sh
```


### Using Docker Containers

#### Clone the code
Clone the [HMC FAIR Data Dashboard](https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/hmc-fair-data-dashboard) public GitLab repository, by
```bash
 git clone https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/hmc-fair-data-dashboard.git
```
or
```bash
 git clone git@codebase.helmholtz.cloud:hmc/hmc-public/FAIR-dashboard/hmc-fair-data-dashboard.git
```


#### Configure of environment variables
Copy the `.env.example` and then fill it with your desire variable
```bash
cd hmc-fair-data-dashboard
cp .env.example .env
```


#### Run the `docker-compose`
```bash copy
sudo docker-compose up
```

Note:

- The `docker` and `docker-compose` should be installed on your machine.
- And for checking you can use: ``$ sudo docker --version`` or ``$ sudo docker-compose --version``, to see if they are
  already installed.



## Support and Contributing

If you are interested in contributing to the project or have any questions not answered here or in
the API documentation, please contact hmc-matter@helmholtz-berlin.de.

### Funding

This work was supported by the [Helmholtz Metadata Collaboration (HMC)](www.helmholtz-metadaten.de), an incubator-platform of the Helmholtz Association within the framework of the Information and Data Science strategic initiative, specifically HMC Hub Matter at the Abteilung Experimentsteuerung und Datenerfassung at the [Helmholtz-Zentrum Berlin für Materialien und Energie GmbH (HZB)](https://ror.org/02aj13c28).

<p>
  <img alt="Logo_HMC" src="https://github.com/Materials-Data-Science-and-Informatics/Logos/blob/main/HMC/HMC_Logo_M.png?raw=true" width="300">

&nbsp;
  <img alt="Logo_HZB" src="https://www.helmholtz-berlin.de/media/design/logo/hzb-logo.svg" width="300">
</p>
