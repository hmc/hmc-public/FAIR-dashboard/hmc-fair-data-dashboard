# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0
# Author: Mojeeb Rahman Sedeqi
import dash
import dash_bootstrap_components as dbc
from dash import Dash
from dash import dcc
from dash import html
from dash import Input
from dash import Output

external_stylesheets = [
    dbc.themes.BOOTSTRAP,
]

app = Dash(__name__, use_pages=True, external_stylesheets=external_stylesheets)
server = app.server
app.config.suppress_callback_exceptions = True

app.layout = dcc.Loading(
    id="loading-general",
    fullscreen=True,
    children=[
        dcc.Location(id="url"),
        dcc.Store(id="viewport_container"),
        html.Div(dash.page_container),
    ],
    type="circle",
)


app.clientside_callback(
    """
    function (href) {
        var w = window.innerWidth;
        var h = window.innerHeight;
        return {'height': h, 'width': w};
    }
    """,
    Output("viewport_container", "data"),
    Input("url", "href"),
)


if __name__ == "__main__":
    # app.run_server(debug=True)
    app.run_server()
