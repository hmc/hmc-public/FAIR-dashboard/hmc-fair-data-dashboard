# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0
import re

import pytest
from playwright.sync_api import expect
from playwright.sync_api import Page

# to add sign commits
# now see that through the pycharm is works or no


@pytest.mark.parametrize(
    "url, title",
    [
        ("http://dashboard:8050", "Welcome"),
        ("http://dashboard:8050/en/about", "About"),
        ("http://dashboard:8050/en/data-in-helmholtz", "Data in Helmholtz"),
        ("http://dashboard:8050/en/fair-by-repository", "Repositories"),
        ("http://dashboard:8050/en/assess-my-data", "Assess My Data"),
        ("http://dashboard:8050/en/about", "About"),
    ],
)
def test_has_title(page: Page, url: str, title: str):
    page.goto(url)

    # Expect a title "to contain" a substring.
    expect(page).to_have_title(re.compile(title), timeout=60000)


@pytest.mark.parametrize(
    "url, name",
    [
        ("http://dashboard:8050/en", "Welcome"),
        ("http://dashboard:8050/en/about", "About"),
        ("http://dashboard:8050/en/data-in-helmholtz", "Data in Helmholtz"),
        ("http://dashboard:8050/en/fair-by-repository", "Repositories"),
        ("http://dashboard:8050/en/assess-my-data", "Assess My Data"),
        ("http://dashboard:8050/en/about", "About"),
    ],
)
def test_get_started_link(page: Page, url: str, name: str):
    page.goto("http://dashboard:8050")
    page.get_by_role("link", name=name).click()
    expect(page).to_have_url(url, timeout=60000)
    expect(page).to_have_title(re.compile(name), timeout=60000)
