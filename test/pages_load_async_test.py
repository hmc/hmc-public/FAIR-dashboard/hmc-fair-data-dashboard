# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0
from playwright.async_api import async_playwright
from playwright.async_api import expect


# goes to next page by simulating a click and checks if it loads correctly by comparing title and url
async def next_page(name, page, url):
    await page.get_by_role("link", name=name).click()
    await expect(page).to_have_url(url)
    await expect(page).to_have_title(name)


async def test_main():
    async with async_playwright() as p:
        i = 0
        while i < 2:
            if i == 0:
                # starts test with firefox browser and creates a new page
                browser = await p.firefox.launch(headless=False)
            else:
                browser = await p.chromium.launch(headless=False)
            page = await browser.new_page()

            # goes to Welcome page and checks if it loads correctly by comparing the title
            await page.goto("https://fairdashboard.helmholtz-metadaten.de/")
            await expect(page).to_have_title("Welcome")

            # Data in Helmholtz page
            await next_page(
                "Data in Helmholtz",
                page,
                "https://fairdashboard.helmholtz-metadaten.de/en/data_in_helmholtz",
            )

            # Repositories page
            await next_page(
                "Repositories",
                page,
                "https://fairdashboard.helmholtz-metadaten.de/en/FAIR-by-repository",
            )

            # My Data page
            await next_page(
                "My Data", page, "https://fairdashboard.helmholtz-metadaten.de/en/data"
            )

            # About page
            await next_page(
                "About", page, "https://fairdashboard.helmholtz-metadaten.de/en/about"
            )
            i = i + 1
