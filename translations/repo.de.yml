# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: CC-BY-4.0

de:
  title: Repositorien
  infoContent: >
    #### Repositorien, in denen Helmholtz-Daten gefunden wurden

    Repositorien bieten wesentliche Infrastruktur, um Forschungsdaten auffindbar und für andere zugänglich zu machen.


    Während die Statistiken auf der [vorherigen Seite](/de/data-in-helmholtz) eine zentrumsspezifische Sicht auf die Daten widerspiegeln,
    liegt der Schwerpunkt dieser Seite auf
    Repositorien-zentrierten Aspekten der Daten.


    Wir haben Helmholtz-Daten in einer Vielzahl von oft  disziplinspezifischen Repositorien gefunden. Bitte wählen Sie ein Repositorium im folgenden Filtermenü aus
    und erfahren Sie, welche Forschungs-Communities in der Helmholtz-Gemeinschaft ihre Daten in dem gewählten
    Repositorium veröffentlichen. Außerdem können Sie einsehen, welche der FAIR-Prinzipien die gefundenen
    Helmholtz-Daten in diesem Repositorium tendenziell besser erfüllen und welche eher weniger.


    Bitte beachten Sie in diesem Zusammenhang Folgendes. FAIR- Prinzipien und FAIR
    Bewertungen sollen Orientierung für die Verbesserung der FAIRness von Daten geben (siehe auch das
    [HMC-Glossar](https://helmholtz-metadaten.de/en/glossary)).  Es gibt verschiedene quantitative und qualitative Methoden zur Bewertung der FAIRness von Daten, aber keine eindeutige Methode. Aus diesem Grund können verschiedene FAIR-Bewertungsinstrumente unterschiedliche Ergebnisse für ein und denselben Datensatz liefern.
    Möglicherweise nehmen wir einige dieser alternativen Methoden in zukünftige Versionen dieses Dashboards auf.

  repositories: Repositorien
  readOn: >
    #### Lesen Sie weiter

    Blättern Sie weiter zur [nächsten Seite](/de/assess-my-data)
    um mehr darüber zu erfahren, was Sie tun können, um
    Ihre Daten für Dritte auffindbar, zugänglich,
    interoperabel und wiederverwendbar zu machen.

  graphResearchFieldInRepositoryTitle: Zielgruppenanalyse für das gewählte Repositorium nach Helmholtz-Forschungsbereich
  graphResearchFieldInRepositoryDes: >
    Jährliches Aufkommen gefundener Helmholtz-Daten in dem ausgewählten Repositorium, aufgelöst nach Zeit und Helmholtz-Forschungsbereich.
    Die gezeigte Statistik beinhaltet Datenveröffentlichungen sowohl durch interne Helmholtz-Mitarbeitende als auch durch
    externen Nutzende von Helmholtz-Forschungseinrichtungen, die durch einen [automatisierten Harvesting-Ansatz](/de/about) gefunden wurden.
    Die Daten sind mit methodenspezifischen Verzerrungen behaftet.
    Für die Richtigkeit und Vollständigkeit dieser Daten kann daher keine Gewähr übernommen werden.
  graphCentersInRepositoryTitle: Zielgruppenanalyse für das gewählte Repositorium nach Helmholtz-Zentrum
  graphCentersInRepositoryDes: >
    Jährliches Aufkommen gefundener Helmholtz-Daten in dem ausgewählten Repositorium, aufgelöst nach Zeit und Helmholtz
    Forschungsbereich.
    Die gezeigte Statistik beinhaltet Datenveröffentlichungen sowohl durch interne Helmholtz-Mitarbeitende als auch durch
    externen Nutzende von Helmholtz-Forschungseinrichtungen, die durch einen [automatisierten Harvesting-Ansatz](/de/about) gefunden wurden.
    Die Daten sind mit methodenspezifischen Verzerrungen behaftet.
    Für die Richtigkeit und Vollständigkeit dieser Daten kann daher keine Gewähr übernommen werden.
  graphAverageFAIRscoreSunburstInRepositoryTitle: Durchschnittliche FAIR-Scores nach F-UJI
  graphAverageFAIRscoreSunburstInRepositoryDes: >
    Interaktive Darstellung der durchschnittlichen FAIR-Scores für die im ausgewählten Repositorium gefundenen Helmholtz-Daten:
    Entdecken Sie Details, indem Sie mit der Maus von innen nach außen über die Grafik fahren
    und beachten Sie die erläuternde Abbildung neben dieser Grafik!


    Alle FAIR-Scores wurden durch [F-UJI](https://doi.org/10.5281/zenodo.4063720) ermittelt,
    welches 13 der 15 FAIR-Prinzipien auf Grundlage von [17 Metriken](https://doi.org/10.5281/zenodo.6461229) bewertet.
    Diese Metriken und Tests werden kontinuierlich weiterentwickelt.
    Da die Auslegung der FAIR-Prinzipien stark von disziplinspezifischen Aspekten abhängen kann,
    können diese Metriken und Tests nicht wirklich beurteilen, wie
    FAIR Daten wirklich sind. Dennoch können sie nützliche Hinweise
    für die Verbesserung bestimmter Aspekte von
    FAIRness geben, insbesondere auf Repositoriumsebene.
  graphAverageFAIRscoreLineInRepositoryTitle: Entwicklung der durchschnittlichen FAIR-Scores für das ausgewählte Repositorium
  graphAverageFAIRscoreLineInRepositoryDes: >
    Zeitliche Entwicklung der durchschnittlichen FAIR-Scores für Helmholtz-Daten, die im gewählten Repositorium gefunden wurden:
    Erkunden Sie die Details durch Anklicken der Filteroptionen am unteren Rand des Diagramms!


    Alle FAIR-Scores wurden durch [F-UJI](https://doi.org/10.5281/zenodo.4063720) ermittelt,
    welches 13 der 15 FAIR-Prinzipien auf Grundlage von [17 Metriken](https://doi.org/10.5281/zenodo.6461229) bewertet.
    Diese Metriken und Tests werden kontinuierlich weiterentwickelt.
    Da die Auslegung der FAIR-Prinzipien stark von disziplinspezifischen Aspekten abhängen kann,
    können diese Metriken und Tests nicht wirklich beurteilen, wie
    FAIR Daten wirklich sind. Dennoch können sie nützliche Hinweise
    für die Verbesserung bestimmter Aspekte von
    FAIRness geben, insbesondere auf Repositoriumsebene.
  indicator: FAIR-Prinzip
  averageFAIRScore: Durchschnittlicher FAIR-Score (F-UJI)
  findableWithDetails: Auffindbarkeit (F) mit Details
  accessibleWithDetails: Zugänglichkeit (A) mit Details
  interoperableWithDetails: Interoperabilität (I) mit Details
  reusableWithDetails: Wiederverwendbarkeit (R) mit Details
  publicationYearLabel: Veröffentlichungsjahr
  researchFieldLabel: Helmholtz-Forschungsbereich
  NumberOfDatasetsLabel: Anzahl der Veröffentlichungen
  centerLabel: Helmholtz-Zentrum
  noFilterMatch: >
    Im ausgewählten Repositorium wurden keine Daten gefunden.
    <br> Bitte treffen Sie eine andere Auswahl.
