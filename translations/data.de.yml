# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: CC-BY-4.0

de:
  title: Offene und FAIRe Daten in Helmholtz
  infoContent: >
    ##### Offene und FAIRe Daten in der Helmholtz-Gemeinschaft

    Offene und FAIRe Daten sind verschiedene, aber eng miteinander zusammenhängende Konzepte.
    Es ist möglich, dass Daten offen sind, nicht aber FAIR; oder FAIR, aber nicht offen.

    ##### Was sind offene Daten?

    Offene Daten sind genau das, was Sie sich darunter vorstellen - Daten, die offen sind!


    Genauer gesagt handelt es sich um Daten, die frei und ohne Einschränkungen - monetärer, technischer oder anderer Art - zugänglich und wiederverwendbar sind.
    Die Daten müssen über eine offene Lizenz verfügen, und sollten in Form und Format so gespeichert werden,
    dass Zugang und Bearbeitung der Daten auf einfache Weise möglich sind. (Siehe auch das [HMC-Glossar](https://helmholtz-metadaten.de/en/glossary))


    ##### Was sind FAIRe Daten?

    FAIR ist die Abkürzung für auffindbar (Findable), zugänglich (Accessible), interoperabel (Interoperable) und
    wiederverwendbar (Reusable) und beschreibt eine Reihe von auf Daten bezogenen Prinzipien.


    Es gibt verschiedene quantitative und qualitative Methoden zur Bewertung der FAIRness von Daten, aber kein eindeutiges Vorgehen.
    Aus diesem Grund können verschiedene FAIR-Bewertungsinstrumente für denselben Datensatz  unterschiedliche Ergebnisse liefern.
    Möglicherweise nehmen wir einige dieser alternativen Methoden in zukünftige Versionen dieses Dashboards auf.


    Bitte beachten Sie, dass die FAIR-Prinzipien und ihre
    Bewertung dazu dienen, Hilfestellung bei der Verbesserung der FAIRness von Daten zu leisten. (Siehe auch das
    [HMC-Glossar](https://helmholtz-metadaten.de/en/glossary))


    ##### Inhalt dieser Seite

    Hier finden Sie interaktive Statistiken zu offenen und FAIRen Daten in der Helmholtz-Gemeinschaft.
    Wählen Sie z.B. einmal einen Helmholtz-Forschungsbereich und/oder ein Helmholtz-Zentrum Ihres Interesses in den
    Filtermenüs aus!

  graphDes: >
    Trendvergleich jährlicher Datenveröffentlichungszahlen in der Helmholtz-Gemeinschaft.
    Die gezeigten Daten umfassen Publikationen sowohl von Helmholtz-Mitarbeitenden als auch externer
    Nutzender von Helmholtz-Forschungseinrichtungen. Die gezeigten Daten wurden durch
    einen [automatisierten Harvesting-Ansatz](/de/about) ermittelt und sind mit methodenspezifischen Verzerrungen behaftet.
    Die Korrektheit oder Vollständigkeit dieser Daten kann daher nicht garantiert werden.

  readOn: >
    #### Lesen Sie weiter

    Blättern Sie weiter zur [nächsten Seite](/de/fair-by-repository) um zu erfahren, welche Repositorien
    von Ihrer Forschungsgemeinschaft genutzt werden und wie Repositorien Sie dabei unterstützen,
    Ihre Daten FAIR zu machen.

  centerLabel: Zentrum
  allCenter: Alle Zentren
  researchFieldLabel: Forschungsfeld
  allResearchField: Alle Forschungsfelder
  graphRepositoryUsageDes: >
    Gesamtzahl gefundener Helmholtz-Datenveröffentlichungen je Repositorium und
    Helmholtz-Forschungsbereich und -Zentrum, wie in den Filtereinstellungen ausgewählt.
    Die gezeigten Daten umfassen Publikationen sowohl von Helmholtz-Mitarbeitenden als auch externer
    Nutzender von Helmholtz-Forschungseinrichtungen. Die gezeigten Daten wurden durch
    einen [automatisierten Harvesting-Ansatz](/de/about) ermittelt und sind mit methodenspezifischen Verzerrungen behaftet.
    Die Korrektheit oder Vollständigkeit dieser Daten kann daher nicht garantiert werden.
  graphRepositoryUsageTitle: Gesamtzahl gefundener Helmholtz-Datenveröffentlichungen je Repositorium
  publishersLabel: Herausgeber
  publisherLabel: Herausgeber
  publicationYearLabel: Publikationsjahr
  NumberOfDatasetsLabel: Anzahl Datenveröffentlichungen
  graphRepositoryOvertimeTitle: Jährliche Gesamtzahl gefundener Helmholtz-Datenveröffentlichungen je Repositorium
  graphRepositoryOvertimeDes: >
    Jährliche Gesamtzahl gefundener Helmholtz-Datenveröffentlichungen
    je Repositorium und Helmholtz-Forschungsbereich und -Zentrum, wie in den Filtereinstellungen ausgewählt.
    Die gezeigten Daten umfassen Publikationen sowohl von Helmholtz-Mitarbeitenden als auch externer
    Nutzender von Helmholtz-Forschungseinrichtungen. Die gezeigten Daten wurden durch
    einen [automatisierten Harvesting-Ansatz](/de/about) ermittelt und sind mit methodenspezifischen Verzerrungen behaftet.
    Die Korrektheit oder Vollständigkeit dieser Daten kann daher nicht garantiert werden.
  noFilterMatch: >
    Für die gewählte Kombination von <br> Helmholtz-Forschungsbereich und -Zentrum <br> wurden keine Daten gefunden.
    Bitte wählen Sie eine andere Kombination oder setzen Sie einen der Filter auf <br> "Alle".
  FujiScoreLabel: F-UJI Score
  AverageFAIR: Mittlerer F-UJI Score
  Findable: Auffindbarkeit (F)
  F: F. Durchschnittlicher F-UJI Score für Auffindbarkeit (F)
  F1: F1. (Meta-)Daten erhalten einen weltweit eindeutigen und dauerhaften Identifikator
  F2: F2. Die Daten werden mit umfangreichen Metadaten beschrieben
  F3: F3. Metadaten enthalten eindeutig und explizit den Identifikator der Daten, die sie beschreiben
  F4: F4. (Meta-)Daten sind in einer durchsuchbaren Ressource registriert oder indiziert
  A: A. Durchschnittlicher F-UJI Score für Zugänglichkeit
  Accessible: Zugänglichkeit (A)
  A1: A1. (Meta-)Daten sind anhand ihres Identifikators über ein standardisiertes Kommunikationsprotokoll abrufbar
  I: I. Durchschnittlicher F-UJI Score für Interoperabilität
  Interoperable: Interoperabilität (I)
  I1: I1. (Meta-)Daten verwenden eine formale, zugängliche, gemeinsame und breit anwendbare Sprache zur Wissensdarstellung.
  I2: I2. (meta)data use vocabularies that follow FAIR principles
  I3: I3. (Meta)daten enthalten qualifizierte Verweise auf andere (Meta)daten
  R: R. Durchschnittlicher F-UJI Score für Wiederverwendbarkeit
  Reusable: Wiederverwendbarkeit (R)
  R1: R1. (Meta-)Daten sind mit einer Vielzahl von genauen und relevanten Attributen reichhaltig beschrieben
  R11: R1.1 (Meta-)Daten werden mit einer klaren und zugänglichen Datennutzungslizenz freigegeben
  R12: R1.2 (Meta-)Daten sind mit einer detaillierten Provenienz verbunden
  R13: R1.3 (Meta)daten erfüllen die für den Bereich relevanten Community-Standards
  Score: Punktzahl
  Weight: Gewichtung
  graphAverageFAIRScoreSunburstTitle: Durchschnittlicher F-UJI Score
  graphAverageFAIRScoreSunburstDes: >
    Interaktive Darstellung der durchschnittlichen F-UJI Scores für Helmholtz-Daten,
    gefiltert nach Repositorums-Anbieter:
    Erkunden Sie die Details, indem Sie mit der Maus von innen nach außen über die Grafik fahren.
    Beachten Sie hierbei die erläuternde Abbildung rechts neben der Grafik!

    Alle Bewertungsergebnisse in dieser Grafik wurden mit Hilfe von
    [F-UJI](https://doi.org/10.5281/zenodo.4063720)
    ermittelt, das 13 der 15 FAIR-Prinzipien auf Grundlage von
    [17 Metriken](https://doi.org/10.5281/zenodo.6461229) auswertet.
    Diese Metriken und Tests werden kontinuierlich weiterentwickelt.
    Da die Auslegung der FAIR-Prinzipien stark von disziplinspezifischen Aspekten abhängen kann,
    können diese Metriken und Bewertungsergebnisse nicht eindeutig beurteilen, wie
    FAIR Daten wirklich sind, aber sie können nützliche Hinweise
    für die Verbesserung bestimmter Aspekte der
    FAIRness geben, insbesondere auf der Repositoriumsebene.
  filter_msg: Die Filteroptionen wurden auf alle Plots auf dieser Seite angewandt.
