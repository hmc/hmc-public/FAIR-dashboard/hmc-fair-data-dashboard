# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: CC-BY-4.0

en:
  title: FAQ
  useThisDashboard: How can I use this dashboard?
  useThisDashboardContent: >
    With this dashboard you can interactively learn about the increasing amounts
    of Open and FAIR data shared by researchers in the Helmholtz Association.
    You can discover the repositories that are used to share research data and find out
    what is required to make data FAIR – Findable, Accessible, Interoperable and
    Reusable by others.
  statisticsObtained: How are these statistics obtained?
  statisticsObtainedContent: >
    A detailed description of how the statistics shown in this dashboard were obtained
    is given in the subpage:
    [About - Background information](/en/about)

  downloadData: Can I download data shown in this dashboard?
  downloadDataContent: >
    The graph shown in this dashboard can be downloaded using
    the small camera-icon on the top right of the figure.
    Graphs can be reused under the license
    [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/deed.en)


    A functionality for downloading data tables is currently
    work in progress and will be offered soon.
  improveTheFAIRness: How can HMC help me to improve the FAIRness of my data?
  improveTheFAIRnessContent: >
    No matter if you are maintaining research data infrastructure,
    a scientific researcher, research data steward, or data reuse
    HMC can help you improve the FAIRness of data you produce, curate, steward, or reuse.


    For scientific researchers and research data stewards we offer regular
    [training courses](https://helmholtz-metadaten.de/en/training-courses)
    and offer a [collection of material](https://helmholtz-metadaten.de/en/training-material-collection)
    for self-directed learning.


    You are welcome to join our monthly
    [FAIR Friday](https://helmholtz-metadaten.de/en/fair-friday)
    lecture series and stay up-to date with everything FAIR.


    We offer a [portfolio](https://helmholtz-metadaten.de/en/tools)
    of tools and services to support you in improving
    the FAIRness of your data. In case you have specific questions do
    not hestitate to contact our
    [HMC helpdesk](mailto:helpdesk@helmholtz-metadaten.de).


    If you are looking for mid- to high-level guidance we have produced a
    [library](https://oceanrep.geomar.de/view/divisions/HMC.html)
    including advice on FAIR implementations, versioning and provenance.
    These are supported by fact sheets explaining the concepts for a general audience.
  contactHMC: How can I contact HMC?
  contactHMCContent: >
    You can contact the dashboard team directly at
    [hmc-matter@helmholtz-berlin.de](mailto:hmc-matter@helmholtz-berlin.de).


    If you have questions about metadata please contact the [HMC helpdesk](mailto:helpdesk@helmholtz-metadaten.de).

  suitableRepository: How can I find a suitable repository?
  suitableRepositoryContent: >
    This page can help you to find out which repositories are used by researchers
    in the community of your interest. Please try and apply the filter-menus
    on top of this page to filter the statistics for the community of your
    interest. You may also be interested in finding out how much an average dataset
    in these repositories adheres to the FAIR data practices. In that case,
    please move on to the tab [Repositories](/en/FAIR-by-repository)
    and select a repository. Please note that registries such as
    [re3data](https://www.re3data.org)
    are another helpful source for discovering research data repositories.

  FAIRprinciples: What are the FAIR principles?
  FAIRprinciplesContent: >
    The FAIR principles were formulated by
    [Wilkinson et al. in 2016](https://doi.org/10.1038/sdata.2016.18).
    These high-level principles provide guidance for improving the Findability,
    Accessibility, Interoperability, and Reusabilty of digital assets
    for both humans and computational systems. A widely used interpretation
    of these principles was [published in 2020](https://doi.org/10.15497/rda00050)
    by the RDA FAIR Data Maturity Model Working Group, resulting in FAIR 41 indicators.

  roleHelmholtzAssociation: What role do the FAIR data principles play in the Helmholtz Association?
  roleHelmholtzAssociationContent: >
    According to the Helmholtz Open Science Policy
    [published in 2022](https://doi.org/10.48440/os.helmholtz.056)
    Helmholtz employees shall ensure that “the digital research data
    that they generate shall be managed responsibly and in accordance with
    the FAIR Principles”. The Helmholtz Metadata Collaboration (HMC) as part
    of the Helmholtz Incubator supports these efforts.

  F-UJIwork: How does the FAIR assessment with F-UJI work?
  F-UJIworkContent: >
    Automated FAIR assessment approaches such as F-UJI can be used as an initial approach to assess the
    the compliance of publicly available datasets
    with respect to specific FAIR principles.
    For this purpose, the [F-UJI](https://doi.org/10.5281/zenodo.4063720)
    framework evaluates 13 of the 15 FAIR principles based on
    [17 metrics](https://doi.org/10.5281/zenodo.6461229).
    These metrics and tests are subject to ongoing development.
    Since the interpretation of the FAIR principles can depend strongly on discipline-specific aspects,
    these metrics and scores cannot truly assess how
    FAIR data really is, but they can provide useful guidance
    for improving certain aspects of
    FAIR, particularly on the repository level.


    Please note that there are various quantitative and qualitative methods to assess the FAIRness of data but no definitive methodology. For this reason different FAIR assessment tools can provide different scores for the same dataset.
    We may include some of these alternate methodologies in future versions of this dashboard.


  interpretingFAIRscores: Why should I be careful when interpreting the FAIR scores shown on this page?
  interpretingFAIRscoresContent: >
    The FAIR scores displayed on this page are the average of the FAIR scores of all Helmholtz data publications,
    that are selected with the filters on this page.
    These represent only a fraction of all data that is hosted in a repository
    and therefore have limited statistical significance.


    The FAIR scores shown in this dashboard were obtained using the
    [F-UJI framework](https://doi.org/10.5281/zenodo.4063720)
    which is based on [17 metrics](https://doi.org/10.5281/zenodo.6461229)
    developed within the European FAIRsFAIR
    consortium. Both, the FAIRsFAIR metrics and the F-UJI framework are
    subject to continuous development. Due to the automatized nature of
    the tests applied by F-UJI, its algorithm has a focus on machine-actionable
    aspects and may be limited with respect to human-understandable aspects of metadata
    as well as regarding discipline-specific aspects.

    Please note that there is no unique way of scoring FAIRness
    (see [Wilkinson et al.](https://doi.org/10.5281/zenodo.7463421)).
    The FAIR principles are a set of high-level principles and applying them depends on
    the specific context.
    You may visit [FAIRassist.org](https://fairassist.org/) and get a glimpse
    of the various possibilities for assessing FAIRness. We may include
    some of these approaches in a future version of this dashboard.

    If you are interested in FAIR assessment for a specific purpose please
    contact the [HMC helpdesk](mailto:helpdesk@helmholtz-metadaten.de) where we will be
    happy to advise you on an appropriate methodology.


  findSuitableRepository: How can I find a suitable repository?
  findSuitableRepositoryContent: >
    This page helps you to understand which research communities
    in the Helmholtz Association already use a given repository and
    an initial assessment of the average FAIR score of these datasets. Please apply the
    filter on top of the page to browse through the list of repositories.
    If you are looking for a repository to publish a dataset in, this
    dashboard can help you to find out which repositories are used by
    the community of your interest. Please move back to the
    [previous page](/en/data_in_helmholtz)
    and apply the filter-menus at the top to filter for the research
    community of your interest. Please note that registries such as
    [re3data](https://www.re3data.org)
    are another helpful source for discovering
    research data repositories.

  reuseCodeDashboard: Can I reuse the code of this dashboard?
  reuseCodeDashboardContent: >
    The source code used for data-harvesting and data-presentation in this dashboard is available [in this GitLab repository](https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard) and is reusable under the [Apache 2.0](https://spdx.org/licenses/Apache-2.0.html) licence.
    A detailed documentation of this project is provided on [https://fairdashboard.helmholtz-metadaten.de/docs](https://fairdashboard.helmholtz-metadaten.de/docs).
