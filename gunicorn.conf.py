# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH, Berlin, Germany
#
# SPDX-License-Identifier: Apache-2.0

"""gunicorn WSGI server configuration."""
import os

from dotenv import find_dotenv
from dotenv import load_dotenv

load_dotenv(find_dotenv())


def ssl_files_exist():
    ssl_cert_file = os.environ.get("SSL_CRT_FILE", "")
    ssl_key_file = os.environ.get("SSL_KEY_FILE", "")

    return os.path.isfile(ssl_cert_file) and os.path.isfile(ssl_key_file)


bind = "0.0.0.0:" + os.environ.get("PORT", "8050")
workers = 3
threads = 1
timeout = 300

if ssl_files_exist():
    certfile = os.environ.get("SSL_CRT_FILE")
    keyfile = os.environ.get("SSL_KEY_FILE")
